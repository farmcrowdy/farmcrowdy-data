package com.farmcrowdyapp.forFeeds;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.farmcrowdyapp.R;

public class FeedsFragment  extends Fragment {
    ProgressBar feedProgressBar;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feeds_layout, container,false);
        feedProgressBar = view.findViewById(R.id.homeProgress);

        RecyclerView recyclerView = view.findViewById(R.id.mainFeedRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        new Handler().postDelayed(() -> {
            feedProgressBar.setVisibility(View.GONE);
            recyclerView.setAdapter(new FeedsAdapter(getContext()));
        }, 2000);

        return view;
    }
}
