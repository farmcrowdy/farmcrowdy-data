package com.farmcrowdyapp.forFeeds;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.farmcrowdyapp.R;

public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.FeedsViewHolder> {
    Context context;
    public FeedsAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override

    public FeedsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.main_feeds_items, viewGroup, false);
        return new FeedsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FeedsViewHolder feedsViewHolder, int i) {
    feedsViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            context.startActivity(new Intent(context, FullInformationActivity.class));
        }
    });
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class FeedsViewHolder extends RecyclerView.ViewHolder {

        public FeedsViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
