package com.farmcrowdyapp;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdyapp.engines.GiveMeData;
import com.farmcrowdyapp.engines.TinyDB;
import com.farmcrowdyapp.forDashboard.DashboardFragment;
import com.farmcrowdyapp.forFarmShops.FarmShopsFragment;
import com.farmcrowdyapp.forFeeds.FeedsFragment;
import com.farmcrowdyapp.forInvite.InvitesFragment;
import com.farmcrowdyapp.forProfile.ProfileFragment;
import com.farmcrowdyapp.onBoarding.LoginActivity;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {

Button doCreate, doLogin;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.ic_home_green_24dp,
            R.drawable.ic_dashboard_green_24dp,
            R.drawable.ic_group_add_green_24dp,
            R.drawable.ic_shopping_cart_green_24dp,
            R.drawable.ic_account_circle_green_24dp
    };
    TextView topText;
    ImageView topImage;
    Spinner sortFarmShops;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(new GiveMeData().getUserId() ==0) {
            startActivity(new Intent(this, LoginActivity.class));
            finishAffinity();
        }

        setContentView(R.layout.activity_main);
        topText = findViewById(R.id.myTopText);
        topImage = findViewById(R.id.topImage);
        viewPager =  findViewById(R.id.mainPager);
        sortFarmShops = findViewById(R.id.sort_farm_shops);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                switch (i) {
                    case 0: topImage.setVisibility(View.VISIBLE);
                        topText.setVisibility(View.GONE);
                        sortFarmShops.setVisibility(View.GONE);
                        break;
                    case 1: topText.setText("Dashboard");
                        topImage.setVisibility(View.GONE);
                        topText.setVisibility(View.VISIBLE);
                        sortFarmShops.setVisibility(View.GONE);
                        break;
                    case 2: topText.setText("Invite");
                        topImage.setVisibility(View.GONE);
                        topText.setVisibility(View.VISIBLE);
                        sortFarmShops.setVisibility(View.GONE);
                        break;
                    case 3:topText.setText("Farm shops");
                        topImage.setVisibility(View.GONE);
                        topText.setVisibility(View.GONE);
                        sortFarmShops.setVisibility(View.VISIBLE);
                        break;
                    case 4: topText.setText("Profile");
                        topImage.setVisibility(View.GONE);
                        topText.setVisibility(View.VISIBLE);
                        sortFarmShops.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        setupViewPager(viewPager);


        tabLayout = findViewById(R.id.mainTabLayout);
        tabLayout.setupWithViewPager(viewPager);

        setupTabIcons();

    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);
        tabLayout.getTabAt(4).setIcon(tabIcons[4]);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new FeedsFragment(), "Feeds");
        adapter.addFrag(new DashboardFragment(), "Dashboard");
        adapter.addFrag(new InvitesFragment(), "Invite");
        adapter.addFrag(new FarmShopsFragment(), "Farm shops");
        adapter.addFrag(new ProfileFragment(), "Profile");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {

            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return 5;
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

}
