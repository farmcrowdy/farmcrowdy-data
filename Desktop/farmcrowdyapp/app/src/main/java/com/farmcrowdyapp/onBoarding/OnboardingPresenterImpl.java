package com.farmcrowdyapp.onBoarding;

import android.util.Log;

import com.farmcrowdyapp.ApplicationInstance;
import com.farmcrowdyapp.LoginPojo;
import com.farmcrowdyapp.MyEndpoint;
import com.farmcrowdyapp.NoPictureInterface;
import com.farmcrowdyapp.engines.TinyDB;
import com.farmcrowdyapp.engines.UserDataPojo;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OnboardingPresenterImpl implements OnboardingPresenter {
    private  NoPictureInterface noPictureInterface;

    public OnboardingPresenterImpl(NoPictureInterface noPictureInterface) {
        this.noPictureInterface = noPictureInterface;
    }

    @Override
    public void checkForLogin(String email, String password) {

        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        RequestBody emailRequest  =  RequestBody.create(MediaType.parse("text/plain"), email.trim());
        RequestBody passwordRequest =  RequestBody.create(MediaType.parse("text/plain"), password);

        Call<LoginPojo> bodyCall = myEndpoint.loginSponsor(emailRequest, passwordRequest);
        bodyCall.enqueue(new Callback<LoginPojo>() {
            @Override
            public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                if(response.isSuccessful()){

                    UserDataPojo myData= response.body().getData();
                    Log.d("MYLOGGER", "NAME IS "+myData.getName() +" with id: "+myData.getUser_id());

                    UserDataPojo userDataPojo = new UserDataPojo();

                    userDataPojo.setUser_id(myData.getUser_id());
                    userDataPojo.setStatus(myData.getStatus());
                    userDataPojo.setRole_id(myData.getRole_id());
                    userDataPojo.setCreated_at(myData.getCreated_at());
                    userDataPojo.setName(myData.getName());

                    userDataPojo.save();

                    noPictureInterface.doSuccess();
                }
                else {
                    noPictureInterface.doFailure("Password or email is incorrect, please check and try again");
                }
            }

            @Override
            public void onFailure(Call<LoginPojo> call, Throwable t) {
                noPictureInterface.doFailure("No internet connection found");
            }
        });

    }

    @Override
    public void checkForRegister(String fname, String lname, String phone, String email, String password) {
        RequestBody emailRequest  =  RequestBody.create(MediaType.parse("text/plain"), email.trim());
        RequestBody passwordRequest =  RequestBody.create(MediaType.parse("text/plain"), password);
        RequestBody nameRequest  =  RequestBody.create(MediaType.parse("text/plain"), fname.trim() + " "+ lname);
      //  RequestBody snameRequest =  RequestBody.create(MediaType.parse("text/plain"), lname.trim());

        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        Call<ResponseBody> bodyCall = myEndpoint.registerSponsor(emailRequest, passwordRequest, nameRequest);
        bodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()) {
                    noPictureInterface.doSuccess(2);
                }
                else noPictureInterface.doFailure("Something wrong while trying to register you. Please try again!");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                noPictureInterface.doFailure("No internet connection found");
            }
        });
    }
}
