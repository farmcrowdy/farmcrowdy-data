package com.farmcrowdyapp;

public interface NoPictureInterface {
    void doSuccess();
    void doFailure(String error);
    void doSuccess(int value);

}
