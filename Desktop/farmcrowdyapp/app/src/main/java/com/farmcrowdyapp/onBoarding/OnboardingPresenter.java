package com.farmcrowdyapp.onBoarding;

public interface OnboardingPresenter {
    void checkForLogin(String email, String password);
    void checkForRegister(String fname, String lname, String phone, String email, String password);
}
