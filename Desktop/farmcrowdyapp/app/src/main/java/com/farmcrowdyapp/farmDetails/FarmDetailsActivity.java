package com.farmcrowdyapp.farmDetails;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.farmcrowdyapp.R;
import com.farmcrowdyapp.checkouts.CheckOutActivity;
import com.farmcrowdyapp.forFarmShops.CommodityDetailsFragment;
import com.farmcrowdyapp.forFarmShops.InsuranceCoverFragment;
import com.farmcrowdyapp.forFarmShops.ReturnsCalculatorFragment;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FarmDetailsActivity extends AppCompatActivity {
    Button sponsorButton, followFarmButton;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.farm_detail_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TabLayout tabLayout = findViewById(R.id.farmDetailsTabLayout);
        ViewPager viewPager = findViewById(R.id.farmDetailsViewPager);
        viewPager.setAdapter(new FarmDetailsPagerAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);

        initViews();
        doClicks();



    }

    void initViews() {
        sponsorButton = findViewById(R.id.farm_details_sponsorNow);
        followFarmButton = findViewById(R.id.farm_details_followFarm);
    }
    void doClicks() {
        sponsorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(FarmDetailsActivity.this, CheckOutActivity.class));
            }
        });
    }

    private class FarmDetailsPagerAdapter extends FragmentPagerAdapter {

        public FarmDetailsPagerAdapter(FragmentManager fm) {
            super(fm);
        }
        String[] tabTitles = {"Product details", "Returns Calculator", "Insurance Cover"};

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        @Override
        public Fragment getItem(int i) {
            Fragment fragment = new Fragment();
            switch (i) {
                case 0: fragment = new CommodityDetailsFragment();
                break;
                case 1: fragment = new ReturnsCalculatorFragment();
                break;
                case 2: fragment = new InsuranceCoverFragment();
                break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
