package com.farmcrowdyapp.forFarmShops;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.farmcrowdyapp.ApplicationInstance;
import com.farmcrowdyapp.MyEndpoint;
import com.farmcrowdyapp.R;
import com.farmcrowdyapp.forFeeds.FeedsAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class FarmShopsFragment  extends Fragment {
    ProgressBar feedProgressBar;
    RecyclerView recyclerView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.farm_shop_fragment_layout, container,false);

        feedProgressBar = view.findViewById(R.id.shopProgress);

        recyclerView = view.findViewById(R.id.shopsecyclerView);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);

        doFetchShops();

        return view;
    }

    void doFetchShops() {
        Observable<ArrayList<FarmShopPojo>> myShops = Observable.create(new Observable.OnSubscribe<ArrayList<FarmShopPojo>>() {
            @Override
            public void call(Subscriber<? super ArrayList<FarmShopPojo>> subscriber) {
                MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
                Call<FarmShopPojoRetrofit> caller = myEndpoint.getAllFarmShops();
                caller.enqueue(new Callback<FarmShopPojoRetrofit>() {
                    @Override
                    public void onResponse(Call<FarmShopPojoRetrofit> call, Response<FarmShopPojoRetrofit> response) {
                        if(response.isSuccessful()) {
                            Log.d("MyError", "very successful");
                            subscriber.onNext(response.body().getData());
                            subscriber.onCompleted();
                        }
                        else {
                            Log.d("MyError", "error due2: "+response.message());
                            subscriber.onNext(null);
                            subscriber.onCompleted();
                        }
                    }

                    @Override
                    public void onFailure(Call<FarmShopPojoRetrofit> call, Throwable t) {
                        Log.d("MyError", "error due2: "+t.getMessage());
                        subscriber.onNext(null);
                        subscriber.onCompleted();
                    }
                });
            }
        });

        myShops.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Action1<ArrayList<FarmShopPojo>>() {
            @Override
            public void call(ArrayList<FarmShopPojo> farmShopPojos) {
                if(farmShopPojos!=null) {
                    feedProgressBar.setVisibility(View.GONE);
                    recyclerView.setAdapter(new FarmShopsAdapter(getContext(), farmShopPojos));
                }
            }
        });
    }
}