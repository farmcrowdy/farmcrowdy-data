package com.farmcrowdyapp;

import android.app.Application;
import android.content.Context;

import com.farmcrowdyapp.engines.TinyDB;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.raizlabs.android.dbflow.config.FlowManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class ApplicationInstance extends Application {
    private static Context context;
    private static Retrofit retrofit;
    private static TinyDB tinyDB;
    private static final String storageBase = "https://www.farmcrowdy.com/storage/farmimages/";
    @Override
    public void onCreate() {
        super.onCreate();

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/product_sans_regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        context = this;
        tinyDB = new TinyDB(context);
        FlowManager.init(this);


        OkHttpClient.Builder httpClientTfs = new OkHttpClient.Builder();
        httpClientTfs.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("X-Authorization", "NyLWdEIlLkVZoUCStljYQsycKxuFbMMtSYmcnkI4YFsxzuKBvOYTRtY5Sazl5GT2"); // <-- this is the important line
                Request request = requestBuilder.build();

                return chain.proceed(request);
            }
        });
        OkHttpClient clientTfs = httpClientTfs.build();
        retrofit = new Retrofit.Builder()
                .client(clientTfs)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://www.farmcrowdy.com/fcrowdy/api/")
                .build();


    }

    public static String getStorageBase() {
        return storageBase;
    }

    public static TinyDB getTinyDB() {
        return tinyDB;
    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }

    public static Context getContext() {
        return context;
    }
}
