package com.farmcrowdyapp.forFarmShops;

public class FarmShopPojo {
    int id;
    String farm_description;
    String statename;
    String roi;
    String price_perunit;
    String status;
    String farm_image;

    public String getFarm_image() {
        return farm_image;
    }

    public void setFarm_image(String farm_image) {
        this.farm_image = farm_image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFarm_description() {
        return farm_description;
    }

    public void setFarm_description(String farm_description) {
        this.farm_description = farm_description;
    }

    public String getStatename() {
        return statename;
    }

    public void setStatename(String statename) {
        this.statename = statename;
    }

    public String getRoi() {
        return roi;
    }

    public void setRoi(String roi) {
        this.roi = roi;
    }

    public String getPrice_perunit() {
        return price_perunit;
    }

    public void setPrice_perunit(String price_perunit) {
        this.price_perunit = price_perunit;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
