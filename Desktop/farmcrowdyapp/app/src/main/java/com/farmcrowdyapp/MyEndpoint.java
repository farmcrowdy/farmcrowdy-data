package com.farmcrowdyapp;

import com.farmcrowdyapp.forFarmShops.FarmShopPojo;
import com.farmcrowdyapp.forFarmShops.FarmShopPojoRetrofit;

import java.util.ArrayList;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface MyEndpoint {

    @Multipart
    @POST("login")
    Call<LoginPojo> loginSponsor(@Part("email") RequestBody email, @Part("password") RequestBody password);

    @Multipart
    @POST("register")
    Call<ResponseBody> registerSponsor(@Part("email") RequestBody email, @Part("password") RequestBody password,
                                  @Part("name") RequestBody name);

    @GET("farmshop")
    Call<FarmShopPojoRetrofit> getAllFarmShops();
}
