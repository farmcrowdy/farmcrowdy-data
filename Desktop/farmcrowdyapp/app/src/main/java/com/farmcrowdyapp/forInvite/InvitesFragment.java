package com.farmcrowdyapp.forInvite;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdyapp.R;

import de.cketti.shareintentbuilder.ShareIntentBuilder;

public class InvitesFragment  extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.invite_fragment, container,false);

        TextView inviteLinkText = view.findViewById(R.id.inviteLinkCode);
        inviteLinkText.setOnClickListener(view1 -> copyNow(inviteLinkText.getText().toString()));

        view.findViewById(R.id.inviteShareImg).setOnClickListener(view1 -> doShareIntent());


        return view;
    }
    void copyNow(String myLink){
        ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("FCInviteCode", myLink);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(getContext(), "Your invite link copied", Toast.LENGTH_SHORT).show();
    }
    void doShareIntent() {
        ShareIntentBuilder.from(getActivity())
                .text("Text to share for example goes here.")
                .share("Share farmcrowdy");
    }
}
