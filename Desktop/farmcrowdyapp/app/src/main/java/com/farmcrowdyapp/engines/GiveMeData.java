package com.farmcrowdyapp.engines;

import com.raizlabs.android.dbflow.sql.language.SQLite;

public class GiveMeData {

    public int getUserId() {
        UserDataPojo userDataPojo = SQLite.select().from(UserDataPojo.class).querySingle();
        return userDataPojo == null ? 0 : userDataPojo.getUser_id();
    }
}
