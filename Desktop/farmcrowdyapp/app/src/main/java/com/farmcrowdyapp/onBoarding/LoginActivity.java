package com.farmcrowdyapp.onBoarding;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.farmcrowdyapp.MainActivity;
import com.farmcrowdyapp.NoPictureInterface;
import com.farmcrowdyapp.R;
import com.farmcrowdyapp.engines.InternetCheck;
import com.farmcrowdyapp.engines.TinyDB;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity implements NoPictureInterface{
    int status = 1;
    MaterialEditText firstnameEdit, lastnameEdit, phonenumberEdit,emailEdit, emailEdit2;
    ShowHidePasswordEditText passwordEdit, passwordEdit2;
    Button doCreate, doLogin;
    CheckBox registerTermsAndConditionsCheckbox;
    OnboardingPresenter onboardingPresenter;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity_layout);

        findViewById(R.id.create_account_text).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.login_placeholder_fragment).setVisibility(View.GONE);
                findViewById(R.id.login_placeholder_fragment_2).setVisibility(View.VISIBLE);
                findViewById(R.id.login_placeholder_fragment_2).setAnimation(AnimationUtils.loadAnimation(LoginActivity.this,R.anim.slider_in_bottom));
                status = 2;
            }
        });


        findViewById(R.id.login_text).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.login_placeholder_fragment_2).setVisibility(View.GONE);
                findViewById(R.id.login_placeholder_fragment).setVisibility(View.VISIBLE);
                findViewById(R.id.login_placeholder_fragment).setAnimation(AnimationUtils.loadAnimation(LoginActivity.this,R.anim.slider_in_bottom));
                status = 1;
            }
        });

        firstnameEdit = findViewById(R.id.first_name);
        lastnameEdit = findViewById(R.id.last_name);
        phonenumberEdit = findViewById(R.id.login_phone);
        emailEdit = findViewById(R.id.login_email);
        emailEdit2 = findViewById(R.id.login_email2);
        passwordEdit = findViewById(R.id.login_password);
        passwordEdit2 = findViewById(R.id.login_password2);
        doCreate = findViewById(R.id.doCreate);
        doLogin = findViewById(R.id.doLogin);
        registerTermsAndConditionsCheckbox = findViewById(R.id.termsBox);
        onboardingPresenter = new OnboardingPresenterImpl(this);
        progressBar = findViewById(R.id.progressbar);
        doCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(firstnameEdit.getText())) firstnameEdit.setError("Please enter your first name");
                else if(TextUtils.isEmpty(lastnameEdit.getText())) lastnameEdit.setError("Please enter your last name");
                else if(TextUtils.isEmpty(phonenumberEdit.getText())) phonenumberEdit.setError("Please enter your phone_number");
                else if(TextUtils.getTrimmedLength(passwordEdit2.getText()) < 6) Toast.makeText(LoginActivity.this, "Password should contain minimum of 6 characters", Toast.LENGTH_SHORT).show();
                else if(!validateEmail(emailEdit2.getText().toString())) emailEdit2.setError("Email address incorrect");
                else if (!registerTermsAndConditionsCheckbox.isChecked()) Toast.makeText(LoginActivity.this, "You need to agree to our terms and conditions", Toast.LENGTH_SHORT).show();
               else  new InternetCheck(internet-> {
                    if(internet)   {
                        onboardingPresenter.checkForRegister(firstnameEdit.getText().toString(),lastnameEdit.getText().toString(),phonenumberEdit.getText().toString(), emailEdit2.getText().toString(), passwordEdit2.getText().toString());
                        doCreate.setText("Registering...");
                        progressBar.setVisibility(View.VISIBLE);
                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    }
                    else {
                        Toast.makeText(LoginActivity.this, "No internet connection, please try again.", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });

        doLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(passwordEdit.getText().length() < 6) Toast.makeText(LoginActivity.this, "Password should contain minimum of 6 characters", Toast.LENGTH_SHORT).show();
                else if(!validateEmail(emailEdit.getText().toString())) emailEdit.setError("Email address incorrect");
                else new InternetCheck(internet-> {
                    if(internet)   {
                        onboardingPresenter.checkForLogin(emailEdit.getText().toString(), passwordEdit.getText().toString());
                        doLogin.setText("Loggin in...");
                        progressBar.setVisibility(View.VISIBLE);
                        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    }
                    else {
                       Toast.makeText(LoginActivity.this, "No internet connection, please try again.", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });











    }

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if(status == 2) {

            findViewById(R.id.login_placeholder_fragment_2).setVisibility(View.GONE);
            findViewById(R.id.login_placeholder_fragment).setVisibility(View.VISIBLE);
            findViewById(R.id.login_placeholder_fragment).setAnimation(AnimationUtils.loadAnimation(LoginActivity.this,R.anim.slider_in_bottom));
            status = 1;

        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public void doSuccess() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        new TinyDB(this).putInt("status", 1);
        progressBar.setVisibility(View.GONE);
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void doFailure(String error) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        new TinyDB(this).putInt("status", 1);
        progressBar.setVisibility(View.GONE);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Failure");
        builder.setMessage(error);
        builder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                dialogInterface.cancel();
            }
        });
        builder.show();

    }

    @Override
    public void doSuccess(int value) {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        new TinyDB(this).putInt("status", 1);
        progressBar.setVisibility(View.GONE);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Successful");
        builder.setMessage("We've sent a validation link to your email address. Kindly check, respond and then come back to login. Thanks!");
        builder.setPositiveButton("Check email now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                dialogInterface.cancel();

                final Intent emailLauncher = new Intent(Intent.ACTION_VIEW);
                emailLauncher.setType("message/rfc822");
                try{
                    startActivity(emailLauncher);
                }catch(ActivityNotFoundException e){
                Toast.makeText(LoginActivity.this, "No email app found, please install one", Toast.LENGTH_SHORT).show();

                }
            }
        });
        builder.show();

    }
}
