package com.farmcrowdyapp.forFarmShops;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.farmcrowdyapp.ApplicationInstance;
import com.farmcrowdyapp.R;
import com.farmcrowdyapp.farmDetails.FarmDetailsActivity;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class FarmShopsAdapter extends RecyclerView.Adapter<FarmShopsAdapter.FarmShopViewHolder> {
    Context context;
    DecimalFormat formatter = new DecimalFormat("#,###,###");
    ArrayList<FarmShopPojo> farmShopPojoArrayList;
    public FarmShopsAdapter(Context context, ArrayList<FarmShopPojo> farmShopPojoArrayList) {
        this.context = context;
        this.farmShopPojoArrayList = farmShopPojoArrayList;
    }

    @NonNull
    @Override
    public FarmShopViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_of_farms_items, viewGroup, false);
        return new FarmShopViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FarmShopViewHolder farmShopViewHolder, int i) {
        FarmShopPojo farmShopPojo = farmShopPojoArrayList.get(i);
        Picasso.with(context).load(ApplicationInstance.getStorageBase()+farmShopPojo.getFarm_image())
                .resize(200,200)
                .into(farmShopViewHolder.farmImage);

        farmShopViewHolder.farmTitle.setText(farmShopPojo.getFarm_description());
        farmShopViewHolder.farmReturns.setText(farmShopPojo.getRoi()+"% returns");
        farmShopViewHolder.farmPrice.setText("NGN "+formatter.format(Integer.valueOf(farmShopPojo.getPrice_perunit())));
        farmShopViewHolder.farmState.setText(farmShopPojo.getStatename());
        farmShopViewHolder.farmStatus.setText(farmShopPojo.getStatus());

        farmShopViewHolder.itemView.setOnClickListener(view-> context.startActivity(new Intent(context, FarmDetailsActivity.class)));
    }

    @Override
    public int getItemCount() {
        if(farmShopPojoArrayList==null) return 0;
        return farmShopPojoArrayList.size();
    }

    public class FarmShopViewHolder extends RecyclerView.ViewHolder {
        ImageView farmImage;
        TextView farmStatus, farmTitle, farmState, farmPrice, farmReturns;
        public FarmShopViewHolder(@NonNull View itemView) {
            super(itemView);
            farmStatus = itemView.findViewById(R.id.feed_farm_status);
            farmTitle = itemView.findViewById(R.id.farm_title);
            farmState = itemView.findViewById(R.id.feed_farm_state);
            farmPrice = itemView.findViewById(R.id.feed_farm_price);
            farmReturns = itemView.findViewById(R.id.feed_farm_returns);
            farmImage = itemView.findViewById(R.id.feed_item_image);
        }
    }
 }
