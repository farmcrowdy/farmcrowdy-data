package com.farmcrowdy.farmcrowdydata.DirectPojo;

public class PagaResponsePojo {
    int responseCode;
    String message,refernceNumber, errorMessage;

    public int getResponseCode() {
        return responseCode;
    }

    public String getMessage() {
        return message;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getRefernceNumber() {
        return refernceNumber;
    }
}
