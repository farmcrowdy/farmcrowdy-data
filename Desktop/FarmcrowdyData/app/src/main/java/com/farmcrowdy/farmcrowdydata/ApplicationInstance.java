package com.farmcrowdy.farmcrowdydata;

import android.app.Application;
import android.content.Context;

import com.google.firebase.FirebaseApp;
//import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ApplicationInstance extends Application {
    public static Context context;
    private static Retrofit retrofit;
    private static FirebaseFirestore firestore;
   // private static FirebaseAuth firebaseAuth;
    @Override
    public void onCreate() {
        super.onCreate();
        context = this;
        FirebaseApp.initializeApp(this);
        firestore = FirebaseFirestore.getInstance();
        FirebaseAnalytics.getInstance(this);
        //firebaseAuth = FirebaseAuth.getInstance();

        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        firestore.setFirestoreSettings(settings);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        FlowManager.init(new FlowConfig.Builder(this).build());
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();
                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("api-key", "aa47f8215c6f30a0dcdb2a36a9f4168e"); // <-- this is the important line
                Request request = requestBuilder.build();

                return chain.proceed(request);
            }
        });
        OkHttpClient client = httpClient.build();

        retrofit = new Retrofit.Builder()
                .client(client)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://farmcrowdy.com/farmapp/api/")
                .build();
    }

    public static Context getContext() { return context; }
    public static Retrofit getRetrofit() {
        return retrofit;
    }
    public static FirebaseFirestore getFirestore() {
        return firestore;
    }

    //public static FirebaseAuth getFirebaseAuth() {
      //  return firebaseAuth;
   // }
}
