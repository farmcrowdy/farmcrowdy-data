package com.farmcrowdy.farmcrowdydata.signUp;


import com.farmcrowdy.farmcrowdydata.ApplicationInstance;
import com.farmcrowdy.farmcrowdydata.MyEndpoint;
import com.farmcrowdy.farmcrowdydata.NoPhotoInterface;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Oluwatobi on 5/25/2018.
 */

public class SignupPresenterImpl implements SignUpPresenter {
    NoPhotoInterface noPhotoInterface;

    public SignupPresenterImpl(NoPhotoInterface noPhotoInterface) {
        this.noPhotoInterface = noPhotoInterface;
    }

    @Override
    public void doRegister(String email, String password, String firsname, String surname) {

        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
        RequestBody emailRequest  =  RequestBody.create(MediaType.parse("text/plain"), email.trim());
        RequestBody passwordRequest =  RequestBody.create(MediaType.parse("text/plain"), password);
        RequestBody fnameRequest  =  RequestBody.create(MediaType.parse("text/plain"), firsname.trim());
        RequestBody snameRequest =  RequestBody.create(MediaType.parse("text/plain"), surname.trim());
        Call<ResponseBody> call  = myEndpoint.addNewUser(emailRequest, passwordRequest, fnameRequest, snameRequest);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.isSuccessful()){
                    if(response.code() == 200) {
                        noPhotoInterface.successful();
                    }
                    else {
                        noPhotoInterface.failure("No internet connection");
                    }
                }
                else{
                    noPhotoInterface.failure(response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                noPhotoInterface.failure(t.getMessage());
            }
        });

    }
}
