package com.farmcrowdy.farmcrowdydata;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdydata.addFarmer.FullFarmerPojo;
import com.farmcrowdy.farmcrowdydata.addFarmer.ProfileActivity;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreFarmGroups;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline;
import com.farmcrowdy.farmcrowdydata.engine.LoadEffecient;
import com.farmcrowdy.farmcrowdydata.engine.TinyDB;
import com.farmcrowdy.farmcrowdydata.login.LoginActivity;
import com.farmcrowdy.farmcrowdydata.seeFarmGroups.CreateFarmGroup;
import com.farmcrowdy.farmcrowdydata.seeFarmGroups.SeeFarmGroupsActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TinyDB tinyDB;
    ProgressDialog progressDialog;
    int counter = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tinyDB = new TinyDB(this);
        progressDialog = new ProgressDialog(this);
        if(tinyDB.getString("user_email") == null || tinyDB.getString("user_email").isEmpty()) startActivity(new Intent(this, LoginActivity.class));

        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        doSomeClickings();
    }
    void doSomeClickings() {
        findViewById(R.id.addFarmer_tabber).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ProfileActivity.class));

            }
        });
        findViewById(R.id.farmGroups_id_tab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ViewOfflineFarmers.class);
                intent.putExtra("groupName", "empty");
                startActivity(intent);
            }
        });
        findViewById(R.id.farmUpdates_tabs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SeeFarmGroupsActivity.class));
            }
        });
        findViewById(R.id.fifthTab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, CreateFarmGroup.class));
            }
        });
        findViewById(R.id.sixTab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Logging out", Toast.LENGTH_SHORT).show();
                new TinyDB(MainActivity.this).clear();
                List<StoreProfileOffline> myStore = SQLite.select().from(StoreProfileOffline.class).queryList();
                List<StoreFarmGroups> myGroups = SQLite.select().from(StoreFarmGroups.class).queryList();
                for(StoreFarmGroups gp: myGroups) {
                    gp.delete();
                }
                for(StoreProfileOffline pf: myStore) {
                    pf.delete();
                }

                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finishAffinity();
            }
        });
        findViewById(R.id.Operations_id_tab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setMessage("Preparing to upload farmer's data online...");
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.setCancelable(false);
                progressDialog.show();
                //ApplicationInstance.getFirebaseAuth().signInAnonymously();

                final List<StoreProfileOffline> myStore = SQLite.select().from(StoreProfileOffline.class).queryList();

                for(StoreProfileOffline profile: myStore) {
                    Bitmap high;
                    counter = counter + 1;
                    FullFarmerPojo fullFarmerPojo = new FullFarmerPojo();
                    fullFarmerPojo.setRole(profile.getRole());
                    fullFarmerPojo.setReadWriteEnglish(profile.isReadWriteEnglish());
                    fullFarmerPojo.setHaveSmartPhone(profile.isHaveSmartPhone());
                    fullFarmerPojo.setUser_add_id(profile.getUser_add_id());
                    fullFarmerPojo.setGovtString(profile.getGovtString());
                    fullFarmerPojo.setYears_of_experience(profile.getYears_of_experience());
                    fullFarmerPojo.setIncome_range(profile.getIncome_range());
                    fullFarmerPojo.setIdNumber(profile.getIdNumber());
                    fullFarmerPojo.setIdType(profile.getIdType());
                    fullFarmerPojo.setLand_area_farmed(profile.getLand_area_farmed());
                    fullFarmerPojo.setBvnNumber(profile.getBvnNumber());
                    fullFarmerPojo.setAcct_number(profile.getAcct_number());
                    fullFarmerPojo.setBankName(profile.getBankName());
                    fullFarmerPojo.setBank_id(profile.getBank_id());
                    fullFarmerPojo.setFname(profile.getFname());
                    fullFarmerPojo.setPhone(profile.getPhone());
                    fullFarmerPojo.setState_id(profile.getState_id());
                    fullFarmerPojo.setDob(profile.getDob());
                    fullFarmerPojo.setPro_image(profile.getPro_image());
                    fullFarmerPojo.setMarital_status(profile.getMarital_status());
                    fullFarmerPojo.setLocal_id(profile.getLocal_id());
                    fullFarmerPojo.setGender(profile.getGender());
                    fullFarmerPojo.setNumber_of_dependants(profile.getNumber_of_dependants());
                    fullFarmerPojo.setFarm_group_id(profile.getFarm_group_id());
                    fullFarmerPojo.setCrop_proficiency(profile.getCrop_proficiency());
                    fullFarmerPojo.setFarmerId(profile.getFarmerId());
                    fullFarmerPojo.setLat1(profile.getLat1());
                    fullFarmerPojo.setLat2(profile.getLat2());
                    fullFarmerPojo.setLat3(profile.getLat3());
                    fullFarmerPojo.setLat4(profile.getLat4());
                    fullFarmerPojo.setLong1(profile.getLong1());
                    fullFarmerPojo.setLong2(profile.getLong2());
                    fullFarmerPojo.setLong3(profile.getLong3());
                    fullFarmerPojo.setLong4(profile.getLong4());
                    fullFarmerPojo.setNextOfKin(profile.getNextOfKin());
                    fullFarmerPojo.setNextOfKinPhone(profile.getNextOfKinPhone());
                    fullFarmerPojo.setTimeStamp(String.valueOf(new Date().getTime()));

                    try {
                        high = new LoadEffecient().decodeSampledBitmapFromFile(profile.getPro_image(), LoadEffecient.maxHeight, LoadEffecient.maxWidth);
                        File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
                        String ts = getPhotoUrl(file);
                        fullFarmerPojo.setPro_image(ts);

                        ApplicationInstance.getFirestore().collection("novusData").add(fullFarmerPojo).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentReference> task) {
                                if(task.isSuccessful()) {
                                    if(counter == myStore.size()) {
                                        List<StoreProfileOffline> myStore = SQLite.select().from(StoreProfileOffline.class).queryList();
                                        List<StoreFarmGroups> myGroups = SQLite.select().from(StoreFarmGroups.class).queryList();

                                        for(StoreProfileOffline pf: myStore) {
                                            pf.delete();
                                        }
                                        progressDialog.dismiss();
                                        progressDialog.cancel();


                                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                        builder.setMessage("Uploading to the server completed successfully");
                                        builder.setTitle("Successful");
                                        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                                dialogInterface.cancel();
                                            }
                                        });
                                        builder.show();
                                    }
                                    else progressDialog.setMessage("Uploading "+counter +" of"+myStore.size()+" completed");

                                }

                                else {
                                    progressDialog.dismiss();
                                    progressDialog.cancel();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                                    builder.setMessage("Something went wrong while uploading...");
                                    builder.setTitle("Failed");
                                    builder.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                            dialogInterface.cancel();
                                        }
                                    });
                                    builder.show();

                                }
                            }
                        });


                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }


                //progressDialog.show();
               // mainPresenter.doSyncOnline();
            }
        });

    }

    private String getPhotoUrl(File file) {
        StorageReference storageRef = FirebaseStorage.getInstance().getReference();
        int photoRan = new Random().nextInt(100);
        int photoRumm = new Random().nextInt(999999);
        String photoName = "fcdy"+photoRan+photoRumm+new Date().getTime();
        final StorageReference mountainsRef = storageRef.child("novus/"+photoName+".jpg");
        UploadTask uploadTask = mountainsRef.putFile(Uri.fromFile(file));

        if(uploadTask.isSuccessful()) {
            return  mountainsRef.getDownloadUrl().toString();
        }
        else {
            return "error";
        }

    }


}
