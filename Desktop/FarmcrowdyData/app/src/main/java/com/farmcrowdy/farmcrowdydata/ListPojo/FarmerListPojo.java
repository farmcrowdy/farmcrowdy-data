package com.farmcrowdy.farmcrowdydata.ListPojo;

import com.farmcrowdy.farmcrowdydata.DirectPojo.FarmerPojo;

import java.util.ArrayList;

/**
 * Created by Oluwatobi on 5/24/2018.
 */

public class FarmerListPojo {
    String status;
    ArrayList<FarmerPojo> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<FarmerPojo> getMessage() {
        return message;
    }
}
