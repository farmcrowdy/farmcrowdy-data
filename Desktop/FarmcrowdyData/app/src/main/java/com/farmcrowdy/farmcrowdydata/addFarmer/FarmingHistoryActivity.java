package com.farmcrowdy.farmcrowdydata.addFarmer;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdydata.MainActivity;
import com.farmcrowdy.farmcrowdydata.R;
import com.google.android.gms.location.LocationRequest;
import com.rafakob.floatingedittext.FloatingEditText;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.vishalsojitra.easylocation.EasyLocationAppCompatActivity;
import com.vishalsojitra.easylocation.EasyLocationRequest;
import com.vishalsojitra.easylocation.EasyLocationRequestBuilder;

import java.util.Date;
import java.util.List;

public class FarmingHistoryActivity extends EasyLocationAppCompatActivity {
    FloatingEditText landArea, yearsOfExperience;
    Spinner incomeRange;
    double genLat, genLong;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_3);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        LocationRequest locationRequest = new LocationRequest()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(2000)
                .setFastestInterval(2000);
        EasyLocationRequest easyLocationRequest = new EasyLocationRequestBuilder()
                .setLocationRequest(locationRequest)
                .setFallBackToLastLocationTime(9000)
                .build();
        requestSingleLocationFix(easyLocationRequest);
        
        final FullFarmerPojo fullFarmerPojo = getIntent().getExtras().getParcelable("data");
        incomeRange = findViewById(R.id.income_range_spinner);
        landArea = findViewById(R.id.farmed_previously);
        yearsOfExperience = findViewById(R.id.years_of_experience_edit);
        
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                incomeRange.getSelectedItem().toString();
                if(landArea.getText().length()>0) {
                    if(!yearsOfExperience.getText().isEmpty()) {
                        fullFarmerPojo.setIncome_range(incomeRange.getSelectedItem().toString());
                        fullFarmerPojo.setLand_area_farmed(landArea.getText());
                        fullFarmerPojo.setYears_of_experience(Integer.valueOf(yearsOfExperience.getText()));
                        
                        saveOffline(fullFarmerPojo);
                    }
                    else {
                        Toast.makeText(FarmingHistoryActivity.this, "Please include years of experience of farmer", Toast.LENGTH_SHORT).show();   
                    }
                }
                else Toast.makeText(FarmingHistoryActivity.this, "Land area cannot be less than 1 hectare", Toast.LENGTH_SHORT).show();
            }
        });
        
        
    }
    private void saveOffline(FullFarmerPojo fullFarmerPojo) {
        List<StoreFarmGroups> myFarmGroups = SQLite.select().from(StoreFarmGroups.class).queryList();
        StoreFarmGroups storeFarmGroups = new StoreFarmGroups();
        storeFarmGroups.setId(myFarmGroups.size()+1);
        storeFarmGroups.setUser_id(fullFarmerPojo.getUser_add_id());
        boolean farmGroupExists = false;
        for(StoreFarmGroups farmGroups: myFarmGroups) {
            if(farmGroups.getGroupName().equals(fullFarmerPojo.getFarm_group_id())){
                farmGroupExists = true;
            }
        }
        if(!farmGroupExists) storeFarmGroups.setGroupName(fullFarmerPojo.getFarm_group_id());

        List<StoreProfileOffline> myStore = SQLite.select().from(StoreProfileOffline.class).queryList();
        StoreProfileOffline storeProfileOffline = new StoreProfileOffline();
        storeProfileOffline.setId(myStore.size()+1);

        storeProfileOffline.setAcct_number(fullFarmerPojo.getAcct_number());
        storeProfileOffline.setBank_id(fullFarmerPojo.getBank_id());
        storeProfileOffline.setCrop_proficiency(fullFarmerPojo.getCrop_proficiency());
        storeProfileOffline.setDob(fullFarmerPojo.getDob());
        storeProfileOffline.setFarm_group_id(fullFarmerPojo.getFarm_group_id());
        storeProfileOffline.setFname(fullFarmerPojo.getFname());
        storeProfileOffline.setGender(fullFarmerPojo.getGender());
        storeProfileOffline.setIncome_range(fullFarmerPojo.getIncome_range());
        storeProfileOffline.setLand_area_farmed(fullFarmerPojo.getLand_area_farmed());
        storeProfileOffline.setYears_of_experience(fullFarmerPojo.getYears_of_experience());
        storeProfileOffline.setNumber_of_dependants(fullFarmerPojo.getNumber_of_dependants());
        storeProfileOffline.setMarital_status(fullFarmerPojo.getMarital_status());
        storeProfileOffline.setState_id(fullFarmerPojo.getState_id());
        storeProfileOffline.setLocal_id(fullFarmerPojo.getLocal_id());
        storeProfileOffline.setUser_add_id(fullFarmerPojo.getUser_add_id());
        storeProfileOffline.setPhone(fullFarmerPojo.getPhone());
        storeProfileOffline.setPro_image(fullFarmerPojo.getPro_image());
        storeProfileOffline.setBankName(fullFarmerPojo.getBankName());
        storeProfileOffline.setBvnNumber(fullFarmerPojo.getBvnNumber());
        storeProfileOffline.setIdType(fullFarmerPojo.getIdType());
        storeProfileOffline.setIdNumber(fullFarmerPojo.getIdNumber());
        storeProfileOffline.setGovtString(fullFarmerPojo.getGovtString());
        storeProfileOffline.setLat1(String.valueOf(genLat));
        storeProfileOffline.setLong1(String.valueOf(genLong));
        storeProfileOffline.setRole(fullFarmerPojo.getRole());
        storeProfileOffline.setHaveSmartPhone(fullFarmerPojo.isHaveSmartPhone());
        storeProfileOffline.setReadWriteEnglish(fullFarmerPojo.isReadWriteEnglish());
        storeProfileOffline.setNextOfKin(fullFarmerPojo.getNextOfKin());
        storeProfileOffline.setNextOfKinPhone(fullFarmerPojo.getNextOfKinPhone());

        String lastTime = String.valueOf(new Date().getTime()).substring(8);

        storeProfileOffline.setFarmerId("FCDY/"+fullFarmerPojo.getUser_add_id()+"/"+lastTime);

        storeProfileOffline.save();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("FCDY/"+fullFarmerPojo.getUser_add_id()+"/"+lastTime);
        builder.setMessage("Above is the farmer's id");
        builder.setCancelable(false);
        builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
           dialogInterface.dismiss();
           dialogInterface.cancel();
                Toast.makeText(FarmingHistoryActivity.this, "Saved offline successfully", Toast.LENGTH_LONG).show();
                startActivity(new Intent(FarmingHistoryActivity.this, MainActivity.class));
                finishAffinity();
            }
        });
        builder.show();




    }

    @Override
    public void onLocationPermissionGranted() {

    }

    @Override
    public void onLocationPermissionDenied() {
    genLat = 0.0;
    genLong = 0.0;
    }

    @Override
    public void onLocationReceived(Location location) {
        genLat = location.getLatitude();
        genLong = location.getLongitude();
    }

    @Override
    public void onLocationProviderEnabled() {

    }

    @Override
    public void onLocationProviderDisabled() {

    }
}
