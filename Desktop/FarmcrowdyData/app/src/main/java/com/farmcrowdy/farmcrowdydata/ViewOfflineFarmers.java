package com.farmcrowdy.farmcrowdydata;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdydata.addFarmer.StoreFarmGroups;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline_Table;
import com.farmcrowdy.farmcrowdydata.engine.LoadEffecient;
import com.farmcrowdy.farmcrowdydata.engine.TinyDB;
import com.farmcrowdy.farmcrowdydata.seeFarmGroups.SeeFarmGroupsActivity;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewOfflineFarmers extends AppCompatActivity {
    RecyclerView recyclerView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_offline_farmers);

        int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);
        String groupName = getIntent().getExtras().getString("groupName");
        List<StoreProfileOffline> myStore = null;
        if(groupName.equals("empty")) myStore = SQLite.select().from(StoreProfileOffline.class).queryList();
        else myStore = SQLite.select().from(StoreProfileOffline.class).where(StoreProfileOffline_Table.farm_group_id.is(groupName)).queryList();

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        recyclerView = findViewById(R.id.offlineFarmersRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new OfflineRecyclerAdapter(myStore));
    }

    private class OfflineRecyclerAdapter extends RecyclerView.Adapter<OfflineRecyclerAdapter.OfflineViewHolder> {
        List<StoreProfileOffline> myStore;
        OfflineRecyclerAdapter(List<StoreProfileOffline> myStore ) {
            this.myStore = myStore;
        }
        @NonNull
        @Override
        public OfflineViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = getLayoutInflater().inflate(R.layout.offline_farmers_items, viewGroup, false);
            return new OfflineViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final OfflineViewHolder offlineViewHolder, int i) {
            offlineViewHolder.nameText.setText(myStore.get(i).getFname());
            offlineViewHolder.text1.setText("Farmer ID: "+myStore.get(i).getFarmerId());
            offlineViewHolder.text2.setText("Farm type: "+myStore.get(i).getCrop_proficiency());
            Bitmap high = null;

            try {
                high = new LoadEffecient().decodeSampledBitmapFromFile(myStore.get(i).getPro_image(), LoadEffecient.maxHeight, LoadEffecient.maxWidth);
                File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
                offlineViewHolder.imageView.setImageURI(Uri.parse(file.getPath()));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            offlineViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ViewOfflineFarmers.this, GeoMappingActivity.class);
                    intent.putExtra("data", myStore.get(offlineViewHolder.getAdapterPosition()).getFarmerId());
                    startActivity(intent);
                }
            });

        }

        @Override
        public int getItemCount() {
            return myStore.size();
        }

        public class OfflineViewHolder extends RecyclerView.ViewHolder {
                CircleImageView imageView;
                TextView nameText, text1, text2;
            public OfflineViewHolder(@NonNull View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.offline_dp);
                nameText = itemView.findViewById(R.id.offline_name);
                text1 = itemView.findViewById(R.id.another1);
                text2 = itemView.findViewById(R.id.another2);
            }
        }
    }
}
