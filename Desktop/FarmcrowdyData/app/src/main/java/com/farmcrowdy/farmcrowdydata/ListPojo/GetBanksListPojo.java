package com.farmcrowdy.farmcrowdydata.ListPojo;


import com.farmcrowdy.farmcrowdydata.DirectPojo.BanksJojo;

import java.util.ArrayList;

/**
 * Created by Oluwatobi on 5/24/2018.
 */

public class GetBanksListPojo {
    String status;

    ArrayList<BanksJojo> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<BanksJojo> getMessage() {
        return message;
    }
}
