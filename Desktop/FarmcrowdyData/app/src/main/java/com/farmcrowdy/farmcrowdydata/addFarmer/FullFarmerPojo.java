package com.farmcrowdy.farmcrowdydata.addFarmer;

import android.os.Parcel;
import android.os.Parcelable;

public class FullFarmerPojo implements Parcelable {
    String fname,role,lat1,lat2,lat3,lat4,long1,long2,long3,long4, farmerId,
    timeStamp,
            govtString, farm_group_id,bankName, bvnNumber,idType, idNumber, dob,dobForPaga,gender,phone,
            marital_status,crop_proficiency,income_range,pro_image,acct_number,land_area_farmed, nextOfKin, nextOfKinPhone;

    int user_add_id,state_id,local_id,farm_location_id,bank_id,
            number_of_dependants,years_of_experience;
    boolean readWriteEnglish, haveSmartPhone;

    public FullFarmerPojo() {
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getLat1() {
        return lat1;
    }

    public void setLat1(String lat1) {
        this.lat1 = lat1;
    }

    public String getLat2() {
        return lat2;
    }

    public void setLat2(String lat2) {
        this.lat2 = lat2;
    }

    public String getLat3() {
        return lat3;
    }

    public void setLat3(String lat3) {
        this.lat3 = lat3;
    }

    public String getLat4() {
        return lat4;
    }

    public void setLat4(String lat4) {
        this.lat4 = lat4;
    }

    public String getLong1() {
        return long1;
    }

    public void setLong1(String long1) {
        this.long1 = long1;
    }

    public String getLong2() {
        return long2;
    }

    public void setLong2(String long2) {
        this.long2 = long2;
    }

    public String getLong3() {
        return long3;
    }

    public void setLong3(String long3) {
        this.long3 = long3;
    }

    public String getLong4() {
        return long4;
    }

    public void setLong4(String long4) {
        this.long4 = long4;
    }

    public String getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(String farmerId) {
        this.farmerId = farmerId;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getGovtString() {
        return govtString;
    }

    public void setGovtString(String govtString) {
        this.govtString = govtString;
    }

    public String getFarm_group_id() {
        return farm_group_id;
    }

    public void setFarm_group_id(String farm_group_id) {
        this.farm_group_id = farm_group_id;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBvnNumber() {
        return bvnNumber;
    }

    public void setBvnNumber(String bvnNumber) {
        this.bvnNumber = bvnNumber;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDobForPaga() {
        return dobForPaga;
    }

    public void setDobForPaga(String dobForPaga) {
        this.dobForPaga = dobForPaga;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public String getCrop_proficiency() {
        return crop_proficiency;
    }

    public void setCrop_proficiency(String crop_proficiency) {
        this.crop_proficiency = crop_proficiency;
    }

    public String getIncome_range() {
        return income_range;
    }

    public void setIncome_range(String income_range) {
        this.income_range = income_range;
    }

    public String getPro_image() {
        return pro_image;
    }

    public void setPro_image(String pro_image) {
        this.pro_image = pro_image;
    }

    public String getAcct_number() {
        return acct_number;
    }

    public void setAcct_number(String acct_number) {
        this.acct_number = acct_number;
    }

    public String getLand_area_farmed() {
        return land_area_farmed;
    }

    public void setLand_area_farmed(String land_area_farmed) {
        this.land_area_farmed = land_area_farmed;
    }

    public String getNextOfKin() {
        return nextOfKin;
    }

    public void setNextOfKin(String nextOfKin) {
        this.nextOfKin = nextOfKin;
    }

    public String getNextOfKinPhone() {
        return nextOfKinPhone;
    }

    public void setNextOfKinPhone(String nextOfKinPhone) {
        this.nextOfKinPhone = nextOfKinPhone;
    }

    public int getUser_add_id() {
        return user_add_id;
    }

    public void setUser_add_id(int user_add_id) {
        this.user_add_id = user_add_id;
    }

    public int getState_id() {
        return state_id;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }

    public int getLocal_id() {
        return local_id;
    }

    public void setLocal_id(int local_id) {
        this.local_id = local_id;
    }

    public int getFarm_location_id() {
        return farm_location_id;
    }

    public void setFarm_location_id(int farm_location_id) {
        this.farm_location_id = farm_location_id;
    }

    public int getBank_id() {
        return bank_id;
    }

    public void setBank_id(int bank_id) {
        this.bank_id = bank_id;
    }

    public int getNumber_of_dependants() {
        return number_of_dependants;
    }

    public void setNumber_of_dependants(int number_of_dependants) {
        this.number_of_dependants = number_of_dependants;
    }

    public int getYears_of_experience() {
        return years_of_experience;
    }

    public void setYears_of_experience(int years_of_experience) {
        this.years_of_experience = years_of_experience;
    }

    public boolean isReadWriteEnglish() {
        return readWriteEnglish;
    }

    public void setReadWriteEnglish(boolean readWriteEnglish) {
        this.readWriteEnglish = readWriteEnglish;
    }

    public boolean isHaveSmartPhone() {
        return haveSmartPhone;
    }

    public void setHaveSmartPhone(boolean haveSmartPhone) {
        this.haveSmartPhone = haveSmartPhone;
    }

    public static Creator<FullFarmerPojo> getCREATOR() {
        return CREATOR;
    }

    protected FullFarmerPojo(Parcel in) {
        fname = in.readString();
        role = in.readString();
        lat1 = in.readString();
        lat2 = in.readString();
        lat3 = in.readString();
        lat4 = in.readString();
        long1 = in.readString();
        long2 = in.readString();
        long3 = in.readString();
        long4 = in.readString();
        farmerId = in.readString();
        timeStamp = in.readString();
        govtString = in.readString();
        farm_group_id = in.readString();
        bankName = in.readString();
        bvnNumber = in.readString();
        idType = in.readString();
        idNumber = in.readString();
        dob = in.readString();
        dobForPaga = in.readString();
        gender = in.readString();
        phone = in.readString();
        marital_status = in.readString();
        crop_proficiency = in.readString();
        income_range = in.readString();
        pro_image = in.readString();
        acct_number = in.readString();
        land_area_farmed = in.readString();
        nextOfKin = in.readString();
        nextOfKinPhone = in.readString();
        user_add_id = in.readInt();
        state_id = in.readInt();
        local_id = in.readInt();
        farm_location_id = in.readInt();
        bank_id = in.readInt();
        number_of_dependants = in.readInt();
        years_of_experience = in.readInt();
        readWriteEnglish = in.readByte() != 0;
        haveSmartPhone = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(fname);
        dest.writeString(role);
        dest.writeString(lat1);
        dest.writeString(lat2);
        dest.writeString(lat3);
        dest.writeString(lat4);
        dest.writeString(long1);
        dest.writeString(long2);
        dest.writeString(long3);
        dest.writeString(long4);
        dest.writeString(farmerId);
        dest.writeString(timeStamp);
        dest.writeString(govtString);
        dest.writeString(farm_group_id);
        dest.writeString(bankName);
        dest.writeString(bvnNumber);
        dest.writeString(idType);
        dest.writeString(idNumber);
        dest.writeString(dob);
        dest.writeString(dobForPaga);
        dest.writeString(gender);
        dest.writeString(phone);
        dest.writeString(marital_status);
        dest.writeString(crop_proficiency);
        dest.writeString(income_range);
        dest.writeString(pro_image);
        dest.writeString(acct_number);
        dest.writeString(land_area_farmed);
        dest.writeString(nextOfKin);
        dest.writeString(nextOfKinPhone);
        dest.writeInt(user_add_id);
        dest.writeInt(state_id);
        dest.writeInt(local_id);
        dest.writeInt(farm_location_id);
        dest.writeInt(bank_id);
        dest.writeInt(number_of_dependants);
        dest.writeInt(years_of_experience);
        dest.writeByte((byte) (readWriteEnglish ? 1 : 0));
        dest.writeByte((byte) (haveSmartPhone ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<FullFarmerPojo> CREATOR = new Creator<FullFarmerPojo>() {
        @Override
        public FullFarmerPojo createFromParcel(Parcel in) {
            return new FullFarmerPojo(in);
        }

        @Override
        public FullFarmerPojo[] newArray(int size) {
            return new FullFarmerPojo[size];
        }
    };
}

