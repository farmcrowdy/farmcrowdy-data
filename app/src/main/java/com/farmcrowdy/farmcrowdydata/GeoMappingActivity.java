package com.farmcrowdy.farmcrowdydata;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline_Table;
import com.google.android.gms.location.LocationRequest;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.vishalsojitra.easylocation.EasyLocationAppCompatActivity;
import com.vishalsojitra.easylocation.EasyLocationRequest;
import com.vishalsojitra.easylocation.EasyLocationRequestBuilder;

import java.math.BigDecimal;
import java.math.RoundingMode;


public class GeoMappingActivity extends EasyLocationAppCompatActivity implements View.OnClickListener{
    TextView movement_geo, answer1, answer2, answer3, answer4, pin1, pin2, pin3, pin4;
    LinearLayout bg1, bg2, bg3, bg4;
    
    double genLat, genLong, lat1, long1, lat2, long2, lat3, long3, lat4, long4;
    StoreProfileOffline farmersData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String farmerId = getIntent().getExtras().getString("data");
        farmersData = SQLite.select().from(StoreProfileOffline.class).where(StoreProfileOffline_Table.farmerId.is(farmerId)).querySingle();



        LocationRequest locationRequest = new LocationRequest()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(2000)
                .setFastestInterval(2000);
        EasyLocationRequest easyLocationRequest = new EasyLocationRequestBuilder()
                .setLocationRequest(locationRequest)
                .setFallBackToLastLocationTime(9000)
                .build();
        requestLocationUpdates(easyLocationRequest);

        setContentView(R.layout.geo_mapping_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        movement_geo = findViewById(R.id.movement_geo);
        
        answer1 = findViewById(R.id.answer_1);
        answer2 = findViewById(R.id.answer_2);
        answer3 = findViewById(R.id.answer_3);
        answer4 = findViewById(R.id.answer_4);
        pin1 = findViewById(R.id.pin1);
        pin2 = findViewById(R.id.pin2);
        pin3 = findViewById(R.id.pin3);
        pin4 = findViewById(R.id.pin4);
        bg1 = findViewById(R.id.bg1);
        bg2 = findViewById(R.id.bg2);
        bg3 = findViewById(R.id.bg3);
        bg4 = findViewById(R.id.bg4);

        ImageView geoImage = findViewById(R.id.geo_img);
        RotateAnimation rotate = new RotateAnimation(0, 360,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);

        rotate.setDuration(4000);
        rotate.setRepeatCount(Animation.INFINITE);
        geoImage.setAnimation(rotate);
        
        pin1.setOnClickListener(this);
        pin2.setOnClickListener(this);
        pin3.setOnClickListener(this);
        pin4.setOnClickListener(this);

    }

    void startEngine() {
        final ProgressDialog builder = new ProgressDialog(this);
        builder.setMessage("Computing coordinates data...");
        builder.setCancelable(false);
        builder.setCanceledOnTouchOutside(false);
        builder.show();

        farmersData.setLat2(String.valueOf(lat2));
        farmersData.setLong2(String.valueOf(long2));

        farmersData.setLat3(String.valueOf(lat3));
        farmersData.setLong3(String.valueOf(long3));

        farmersData.setLat4(String.valueOf(lat4));
        farmersData.setLong4(String.valueOf(long4));
        farmersData.update();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                builder.dismiss();
                builder.cancel();

                AlertDialog.Builder alert = new AlertDialog.Builder(GeoMappingActivity.this);
                alert.setMessage("Four point geo-coordinates taken successfully");
                alert.setTitle("Successful");
                alert.setCancelable(false);
                alert.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    dialogInterface.cancel();
                    finish();  }
                });
                alert.show();


            }
        }, 3000);
    }

    @Override
    public void onLocationPermissionGranted() {

    }

    @Override
    public void onLocationPermissionDenied() {

    }

    @Override
    public void onLocationReceived(Location location) {
        genLat = location.getLatitude();
        genLong = location.getLongitude();
        movement_geo.setText(String.format("Lat: %s Long: %s", genLat, genLong));
        movement_geo.setAnimation(AnimationUtils.loadAnimation(this, android.R.anim.fade_in));
    }

    @Override
    public void onLocationProviderEnabled() {

    }

    @Override
    public void onLocationProviderDisabled() {

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.pin1:
                lat1 = genLat;
                long1 = genLong;
                answer1.setText(String.format("POINT A \n"+"Lat: %s Long: %s", genLat, genLong));
                bg1.setBackgroundColor(getResources().getColor(R.color.PPR_BLUE_GREY300));
                pin1.setBackgroundColor(getResources().getColor(R.color.PPR_BLUE_GREY400));
                pin1.setText("Re-Pin");
                bg2.setVisibility(View.VISIBLE);
                farmersData.setLat1(String.valueOf(lat1));
                farmersData.setLong1(String.valueOf(long1));
                farmersData.update();
                Toast.makeText(this, "Initial coordinate updated", Toast.LENGTH_SHORT).show();


                break;
            case R.id.pin2:
                lat2 = genLat;
                long2 = genLong;
                answer2.setText(String.format("POINT B \n"+"Lat: %s Long: %s", genLat, genLong));
                bg2.setBackgroundColor(getResources().getColor(R.color.PPR_BLUE_GREY300));
                pin2.setBackgroundColor(getResources().getColor(R.color.PPR_BLUE_GREY400));
                pin2.setText("Re-Pin");
                bg3.setVisibility(View.VISIBLE);
                break;
            case R.id.pin3:
                lat3 =  genLat;
                long3 =  genLong;
                answer3.setText(String.format("POINT C \n"+"Lat: %s Long: %s", genLat, genLong));
                bg3.setBackgroundColor(getResources().getColor(R.color.PPR_BLUE_GREY300));
                pin3.setBackgroundColor(getResources().getColor(R.color.PPR_BLUE_GREY400));
                pin3.setText("Re-Pin");
                bg4.setVisibility(View.VISIBLE);
                break;
            case R.id.pin4:
                lat4 = genLat;
                long4 = genLong;
                answer4.setText(String.format("POINT D \n"+"Lat: %s Long: %s", genLat, genLong));
                bg4.setBackgroundColor(getResources().getColor(R.color.PPR_BLUE_GREY300));
                pin4.setBackgroundColor(getResources().getColor(R.color.PPR_BLUE_GREY400));
                pin4.setText("Re-Pin");
                startEngine();
                break;
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

}


