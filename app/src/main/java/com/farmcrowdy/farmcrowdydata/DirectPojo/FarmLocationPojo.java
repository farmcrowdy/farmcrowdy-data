package com.farmcrowdy.farmcrowdydata.DirectPojo;

import com.farmcrowdy.farmcrowdydata.engine.MyDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Oluwatobi on 5/25/2018.
 */
@Table(database = MyDatabase.class)
public class FarmLocationPojo extends BaseModel{
    @Column
    @PrimaryKey
    int location_id;

    @Column
    String location;

    public int getLocation_id() {
        return location_id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
