package com.farmcrowdy.farmcrowdydata.DirectPojo;

import com.farmcrowdy.farmcrowdydata.engine.MyDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Oluwatobi on 5/25/2018.
 */
@Table(database = MyDatabase.class)
public class LocalGovtPojo extends BaseModel {
    @Column
    @PrimaryKey
    int local_id;
    @Column
    int state_id;
    @Column
    String local_name;

    public int getLocal_id() {
        return local_id;
    }

    public int getState_id() {
        return state_id;
    }

    public String getLocal_name() {
        return local_name;
    }

    public void setLocal_id(int local_id) {
        this.local_id = local_id;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }

    public void setLocal_name(String local_name) {
        this.local_name = local_name;
    }
}
