package com.farmcrowdy.farmcrowdydata.DirectPojo;

import com.farmcrowdy.farmcrowdydata.engine.MyDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

/**
 * Created by Oluwatobi on 5/25/2018.
 */
@Table(database = MyDatabase.class)
public class CropPojo extends BaseModel {
    @Column
    @PrimaryKey
    int crop_id;
    @Column
    String crop_name;

    public int getCrop_id() {
        return crop_id;
    }

    public String getCrop_name() {
        return crop_name;
    }

    public void setCrop_id(int crop_id) {
        this.crop_id = crop_id;
    }

    public void setCrop_name(String crop_name) {
        this.crop_name = crop_name;
    }
}
