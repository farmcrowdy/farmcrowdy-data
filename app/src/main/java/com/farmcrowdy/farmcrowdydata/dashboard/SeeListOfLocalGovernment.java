package com.farmcrowdy.farmcrowdydata.dashboard;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdydata.ApplicationInstance;
import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.addFarmer.FullFarmerPojo;
import com.farmcrowdy.farmcrowdydata.localGovtFarmers.LocalGovtActivity;

import java.util.ArrayList;

public class SeeListOfLocalGovernment extends AppCompatActivity{
    RecyclerView recyclerView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_offline_farmers);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        int chosenState = getIntent().getExtras().getInt("data", 1);
        Log.d("Dasher", "Chosen state is: "+chosenState);
        ArrayList<String> localGovtList = new ArrayList<>();

        for(FullFarmerPojo pojo: ApplicationInstance.getFullFarmerPojoList()) {
            if(pojo.getState_id() == chosenState) {
               if(!localGovtList.contains(pojo.getGovtString())) localGovtList.add(pojo.getGovtString());
            }
        }



        recyclerView = findViewById(R.id.offlineFarmersRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new ListOfGovtAdapter(localGovtList));

    }

    class ListOfGovtAdapter extends RecyclerView.Adapter<ListOfGovtAdapter.GovtViewHolder> {
        ArrayList<String> localList;

         ListOfGovtAdapter(ArrayList<String> localList) {
            this.localList = localList;
        }

        @NonNull
        @Override
        public GovtViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_text__items, viewGroup, false);
            return new GovtViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final GovtViewHolder govtViewHolder, int i) {
                govtViewHolder.singleText.setText(localList.get(i));
                govtViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(SeeListOfLocalGovernment.this, LocalGovtActivity.class);
                        intent.putExtra("data", localList.get(govtViewHolder.getAdapterPosition()));
                        startActivity(intent);
                    }
                });
        }

        @Override
        public int getItemCount() {
            if(localList ==null) return 0;
            return localList.size();
        }

        class GovtViewHolder extends RecyclerView.ViewHolder {
            TextView singleText;
           GovtViewHolder(@NonNull View itemView) {
                super(itemView);
                singleText = itemView.findViewById(R.id.single_text);
            }
        }
    }
}
