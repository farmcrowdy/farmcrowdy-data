package com.farmcrowdy.farmcrowdydata.dashboard;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdydata.ApplicationInstance;
import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.addFarmer.FullFarmerPojo;
import com.github.curioustechizen.ago.RelativeTimeTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ListOfAgents extends AppCompatActivity{
    RecyclerView recyclerView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_offline_farmers);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        recyclerView = findViewById(R.id.offlineFarmersRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new ListOfAgentsRecyclerAdapter());
    }

    class ListOfAgentsRecyclerAdapter extends  RecyclerView.Adapter<ListOfAgentsRecyclerAdapter.AgentsViewHolder> {
        @NonNull
        @Override
        public AgentsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_agents_items, viewGroup, false);
            return new AgentsViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull AgentsViewHolder agentsViewHolder, int i) {
            final EachAgents eachAgents = ApplicationInstance.getAllAgents().get(i);
            agentsViewHolder.agentName.setText(eachAgents.getFirstname()+" "+eachAgents.getSurname());
            agentsViewHolder.agentEmail.setText(eachAgents.getEmail());
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            try {
                Date d = f.parse(eachAgents.getDate());
                long milliseconds = d.getTime();
                agentsViewHolder.agentRegistered.setReferenceTime(milliseconds);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            agentsViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final ArrayList<FullFarmerPojo> farmersList = new ArrayList<>();
                    for(FullFarmerPojo pojo: ApplicationInstance.getFullFarmerPojoList()) {
                        if(pojo.getUser_add_id() == eachAgents.getUser_id()) farmersList.add(pojo);
                    }
                    Intent intent = new Intent(ListOfAgents.this, ListOfAllFamersBasedOnParams.class);
                    intent.putExtra("data", farmersList);
                    startActivity(intent);
                }
            });

        }

        @Override
        public int getItemCount() {
            if(ApplicationInstance.getAllAgents() == null) return 0;
            else return ApplicationInstance.getAllAgents().size();
        }

        class AgentsViewHolder extends RecyclerView.ViewHolder {

            TextView agentName, agentEmail;
            RelativeTimeTextView agentRegistered;
            public AgentsViewHolder(@NonNull View itemView) {
                super(itemView);
                agentName = itemView.findViewById(R.id.viewAgent_name);
                agentEmail = itemView.findViewById(R.id.viewAgent_email);
                agentRegistered = itemView.findViewById(R.id.viewAgent_stamp);
            }
        }
    }


}
