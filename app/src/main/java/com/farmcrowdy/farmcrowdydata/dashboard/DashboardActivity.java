package com.farmcrowdy.farmcrowdydata.dashboard;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdydata.ApplicationInstance;
import com.farmcrowdy.farmcrowdydata.ListFullfarmer;
import com.farmcrowdy.farmcrowdydata.MyEndpoint;
import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.addFarmer.FullFarmerPojo;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.hadiidbouk.charts.BarData;
import com.hadiidbouk.charts.ChartProgressBar;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.razerdp.widget.animatedpieview.AnimatedPieView;
import com.razerdp.widget.animatedpieview.AnimatedPieViewConfig;
import com.razerdp.widget.animatedpieview.data.SimplePieInfo;
import com.tobilvr.works.peppercolor.PepperColors;
import com.tobilvr.works.peppercolor.PepperSingleColor;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.ObservableEmitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardActivity extends AppCompatActivity implements BackUpInterface{
    ChartProgressBar mChart, cropChart, mChartStates2, cropChart2;
    TextView totalFarmers, totalDependants, totalAgents,totalFarmGroups,landArea;
    LinearLayout myProgress;
    NestedScrollView fullLayout;
    int totalBackedup = 0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_activity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> finish());

        totalFarmers = findViewById(R.id.totalFarmersText);
        totalDependants = findViewById(R.id.totalNumberOfDependants);
        totalAgents = findViewById(R.id.numberOfAgents);
        totalFarmGroups = findViewById(R.id.totalNumberFarmGroupsText);
        landArea = findViewById(R.id.landAreaText);

        myProgress = findViewById(R.id.myProgress);
        fullLayout = findViewById(R.id.fullLayout);



        //loadAllFarmersData();
        //setUpBackUpRx();
        loadFarmersFromAzure();
        //doSaveOfflineFirst(null);

    }

    @SuppressLint("CheckResult")
    void setUpBackUpRx() {
        ProgressDialog progressDialog  = new ProgressDialog(this);
        progressDialog.setMessage("Backing up");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
                progressDialog.cancel();
            }
        }, 10000);

        BackUpPresenterImpl backUpPresenter = new BackUpPresenterImpl(this);
        backUpPresenter.startWork();



    }

   void  doSaveOfflineFirst( ObservableEmitter<Integer> emitter) {
        List<StoreProfileOffline> myStore = SQLite.select().from(StoreProfileOffline.class).queryList();
        Log.d("BACKUP", "total profiles: "+myStore.size());
    }

    void loadFarmersFromAzure() {
        Log.d("DASHER", "enters here");
        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);

        Call<ListFullfarmer> caller = myEndpoint.getFarmersFromAzure();
        caller.enqueue(new Callback<ListFullfarmer>() {
            @Override
            public void onResponse(Call<ListFullfarmer> call, Response<ListFullfarmer> response) {

                if(!response.isSuccessful()) {
                    Log.d("DASHBOARD_FIX", "Dashobard failed small due to: "+response.message());
                    return;
                }
                else {
                    Log.d("DASHBOARD_FIX", "Dashobard loaded well");
                }

                myProgress.setVisibility(View.GONE);
                fullLayout.setVisibility(View.VISIBLE);

                List<FullFarmerPojo> fullFarmerPojoList = response.body().getMessage();
                Log.d("DASHBOARD_FIX", "Dashobard loaded size"+fullFarmerPojoList.size());
                new ApplicationInstance().setFullFarmerPojoList(fullFarmerPojoList);

                int totalLandArea = 0;
                int numDependants = 0;
                ArrayList<String> listOfFarmGroups = new ArrayList<>();

                int sokotoFarmers = 0;
                int gombeFarmers = 0;
                int nigerFarmers = 0;
                int ogunFarmers = 0;
                int jigawaFarmers = 0;
                int edoFarmers = 0;
                int kadunaFarmers = 0;
                int tarabaFarmers = 0;
                int kebbiFarmers = 0;
                int deltaFarmers = 0;

                int riceCrop = 0;
                int sorghumCrop = 0;
                int soybeanCrop = 0;
                int maizeCrop = 0;
                int cassavaCrop = 0;

                int withSmartPhone = 0;
                int canReadEnglish = 0;
                int isMale = 0;


                for (FullFarmerPojo fullFarmerPojo : fullFarmerPojoList) {
                    totalLandArea = Integer.valueOf(fullFarmerPojo.getLand_area_farmed()) + totalLandArea;

                    if (!listOfFarmGroups.contains(fullFarmerPojo.getFarm_group_id())) listOfFarmGroups.add(fullFarmerPojo.getFarm_group_id());
                    if(fullFarmerPojo.isHaveSmartPhone()) withSmartPhone = withSmartPhone + 1;
                    if(fullFarmerPojo.isReadWriteEnglish()) canReadEnglish = canReadEnglish + 1;
                    if(fullFarmerPojo.getGender().equals("Male")) isMale = isMale + 1;


                    numDependants = numDependants + fullFarmerPojo.getNumber_of_dependants();
                    if (fullFarmerPojo.getState_id() == 34) sokotoFarmers = sokotoFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 16) gombeFarmers = gombeFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 27) nigerFarmers = nigerFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 28) ogunFarmers = ogunFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 18) jigawaFarmers = jigawaFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 12) edoFarmers = edoFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 19) kadunaFarmers = kadunaFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 35) tarabaFarmers = tarabaFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 22) kebbiFarmers = kebbiFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 10) deltaFarmers = deltaFarmers + 1;

                    if(fullFarmerPojo.getCrop_proficiency().equals("Rice")) riceCrop = riceCrop + 1;
                    if(fullFarmerPojo.getCrop_proficiency().equals("Sorghum")) sorghumCrop = sorghumCrop + 1;
                    if(fullFarmerPojo.getCrop_proficiency().equals("Soybean")) soybeanCrop = soybeanCrop + 1;
                    if(fullFarmerPojo.getCrop_proficiency().equals("Maize")) maizeCrop = maizeCrop + 1;
                    if(fullFarmerPojo.getCrop_proficiency().equals("Cassava")) cassavaCrop = cassavaCrop + 1;


                }
                int withoutSmartPhone = fullFarmerPojoList.size()  - withSmartPhone;
                int illitrate = fullFarmerPojoList.size() - canReadEnglish;
                int isFemale = fullFarmerPojoList.size() - isMale;


                totalFarmers.setText(String.valueOf(fullFarmerPojoList.size() - 11));
                landArea.setText(String.valueOf(totalLandArea+" Ha" ));
                totalFarmGroups.setText(String.valueOf(listOfFarmGroups.size()));
                totalDependants.setText(String.valueOf(numDependants));
                //Load Bar Chart
                loadStatesBarChart(sokotoFarmers, gombeFarmers, nigerFarmers, ogunFarmers, jigawaFarmers, edoFarmers, kadunaFarmers);
                loadStateTwoBarChart(tarabaFarmers, kebbiFarmers, deltaFarmers);

                loadPieChart(withSmartPhone, withoutSmartPhone, canReadEnglish, illitrate,isMale, isFemale);


                loadCropsBarChart(riceCrop, sorghumCrop, maizeCrop,cassavaCrop,soybeanCrop);

                doClickers(listOfFarmGroups, fullFarmerPojoList);
                doGroupClick(sokotoFarmers, gombeFarmers, nigerFarmers, ogunFarmers, jigawaFarmers, edoFarmers, kadunaFarmers, kebbiFarmers, deltaFarmers, tarabaFarmers);

                MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
                Call<ListOfAgentsPojo> caller = myEndpoint.getAllAgents();
                caller.enqueue(new Callback<ListOfAgentsPojo>() {
                    @Override
                    public void onResponse(Call<ListOfAgentsPojo> call, Response<ListOfAgentsPojo> response) {
                        if (response.isSuccessful()) {
                            totalAgents.setText(String.valueOf(response.body().getMessage().size() - 19));
                            new ApplicationInstance().setAllAgents(response.body().getMessage());
                        } else totalAgents.setText("NIL");
                    }

                    @Override
                    public void onFailure(Call<ListOfAgentsPojo> call, Throwable t) {
                        totalAgents.setText("NIL");
                    }
                });
            }

            @Override
            public void onFailure(Call<ListFullfarmer> call, Throwable t) {
                    Log.d("DASHBOARD_FIX", "Dashobard failed pataptat due to: "+t.getMessage());
            }
        });
    }

    void loadAllFarmersData() {
        ApplicationInstance.getFirestore().collection("novusData").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@javax.annotation.Nullable QuerySnapshot queryDocumentSnapshots, @javax.annotation.Nullable FirebaseFirestoreException e) {
                if (e == null){
                    myProgress.setVisibility(View.GONE);
                    fullLayout.setVisibility(View.VISIBLE);
                    List<FullFarmerPojo> fullFarmerPojoList = queryDocumentSnapshots.toObjects(FullFarmerPojo.class);
                    new ApplicationInstance().setFullFarmerPojoList(fullFarmerPojoList);

                int totalLandArea = 0;
                int numDependants = 0;
                ArrayList<String> listOfFarmGroups = new ArrayList<>();

                int sokotoFarmers = 0;
                int gombeFarmers = 0;
                int nigerFarmers = 0;
                int ogunFarmers = 0;
                int jigawaFarmers = 0;
                int edoFarmers = 0;
                int kadunaFarmers = 0;
                int tarabaFarmers = 0;
                int kebbiFarmers = 0;
                int deltaFarmers = 0;

                int riceCrop = 0;
                int sorghumCrop = 0;
                int soybeanCrop = 0;
                int maizeCrop = 0;
                int cassavaCrop = 0;

                int withSmartPhone = 0;
                int canReadEnglish = 0;
                int isMale = 0;


                for (FullFarmerPojo fullFarmerPojo : fullFarmerPojoList) {
                    totalLandArea = Integer.valueOf(fullFarmerPojo.getLand_area_farmed()) + totalLandArea;

                    if (!listOfFarmGroups.contains(fullFarmerPojo.getFarm_group_id())) listOfFarmGroups.add(fullFarmerPojo.getFarm_group_id());
                    if(fullFarmerPojo.isHaveSmartPhone()) withSmartPhone = withSmartPhone + 1;
                    if(fullFarmerPojo.isReadWriteEnglish()) canReadEnglish = canReadEnglish + 1;
                    if(fullFarmerPojo.getGender().equals("Male")) isMale = isMale + 1;


                    numDependants = numDependants + fullFarmerPojo.getNumber_of_dependants();
                    if (fullFarmerPojo.getState_id() == 34) sokotoFarmers = sokotoFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 16) gombeFarmers = gombeFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 27) nigerFarmers = nigerFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 28) ogunFarmers = ogunFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 18) jigawaFarmers = jigawaFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 12) edoFarmers = edoFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 19) kadunaFarmers = kadunaFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 35) tarabaFarmers = tarabaFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 22) kebbiFarmers = kebbiFarmers + 1;
                    if (fullFarmerPojo.getState_id() == 10) deltaFarmers = deltaFarmers + 1;

                    if(fullFarmerPojo.getCrop_proficiency().equals("Rice")) riceCrop = riceCrop + 1;
                    if(fullFarmerPojo.getCrop_proficiency().equals("Sorghum")) sorghumCrop = sorghumCrop + 1;
                    if(fullFarmerPojo.getCrop_proficiency().equals("Soybean")) soybeanCrop = soybeanCrop + 1;
                    if(fullFarmerPojo.getCrop_proficiency().equals("Maize")) maizeCrop = maizeCrop + 1;
                    if(fullFarmerPojo.getCrop_proficiency().equals("Cassava")) cassavaCrop = cassavaCrop + 1;


                }
                int withoutSmartPhone = fullFarmerPojoList.size()  - withSmartPhone;
                int illitrate = fullFarmerPojoList.size() - canReadEnglish;
                int isFemale = fullFarmerPojoList.size() - isMale;


                totalFarmers.setText(String.valueOf(fullFarmerPojoList.size() - 11));
                landArea.setText(String.valueOf(totalLandArea+" Ha" ));
                totalFarmGroups.setText(String.valueOf(listOfFarmGroups.size()));
                totalDependants.setText(String.valueOf(numDependants));
                //Load Bar Chart
                loadStatesBarChart(sokotoFarmers, gombeFarmers, nigerFarmers, ogunFarmers, jigawaFarmers, edoFarmers, kadunaFarmers);
                loadStateTwoBarChart(tarabaFarmers, kebbiFarmers, deltaFarmers);

                loadPieChart(withSmartPhone, withoutSmartPhone, canReadEnglish, illitrate,isMale, isFemale);


                loadCropsBarChart(riceCrop, sorghumCrop, maizeCrop,cassavaCrop,soybeanCrop);

                doClickers(listOfFarmGroups, fullFarmerPojoList);
                doGroupClick(sokotoFarmers, gombeFarmers, nigerFarmers, ogunFarmers, jigawaFarmers, edoFarmers, kadunaFarmers, kebbiFarmers, deltaFarmers, 3);

                MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
                Call<ListOfAgentsPojo> caller = myEndpoint.getAllAgents();
                caller.enqueue(new Callback<ListOfAgentsPojo>() {
                    @Override
                    public void onResponse(Call<ListOfAgentsPojo> call, Response<ListOfAgentsPojo> response) {
                        if (response.isSuccessful()) {
                            totalAgents.setText(String.valueOf(response.body().getMessage().size() - 19));
                            new ApplicationInstance().setAllAgents(response.body().getMessage());
                        } else totalAgents.setText("NIL");
                    }

                    @Override
                    public void onFailure(Call<ListOfAgentsPojo> call, Throwable t) {
                        totalAgents.setText("NIL");
                    }
                });
            }
            else {
                    Toast.makeText(DashboardActivity.this, "Something went wrong, please try again later", Toast.LENGTH_LONG);
                    finish();
                }
            }
        });
    }

    void loadStatesBarChart(int sokoto, int gombe, int niger,int ogun, int jigawa, int edo, int kaduna) {
        ArrayList<BarData> dataList = new ArrayList<>();

        BarData data = new BarData("Sokoto ("+sokoto+")", Float.valueOf(String.valueOf(sokoto)), ""+sokoto);
        dataList.add(data);

        data = new BarData("Gombe ("+gombe+")", Float.valueOf(String.valueOf(gombe)), ""+gombe);
        dataList.add(data);

        data = new BarData("Niger ("+niger+")", Float.valueOf(String.valueOf(niger)), ""+niger);
        dataList.add(data);

        data = new BarData("Ogun ("+ogun+")", Float.valueOf(String.valueOf(ogun)), ""+ogun);
        dataList.add(data);

        data = new BarData("Jigawa ("+jigawa+")", Float.valueOf(String.valueOf(jigawa)), ""+jigawa);
        dataList.add(data);

        data = new BarData("Edo ("+edo+")", Float.valueOf(String.valueOf(edo)), ""+edo);
        dataList.add(data);

        data = new BarData("Kaduna ("+kaduna+")", Float.valueOf(String.valueOf(edo)), ""+kaduna);
        dataList.add(data);

        mChart = findViewById(R.id.ChartProgressBar);

        mChart.setDataList(dataList);
        mChart.build();

    }

    void loadStateTwoBarChart(int taraba, int kebbi, int delta) {
        ArrayList<BarData> dataList = new ArrayList<>();

        BarData data = new BarData("Taraba ("+taraba+")", Float.valueOf(String.valueOf(taraba)), ""+taraba);
        dataList.add(data);

        data = new BarData("Kebbi ("+kebbi+")", Float.valueOf(String.valueOf(kebbi)), ""+kebbi);
        dataList.add(data);

        data = new BarData("Delta ("+delta+")", Float.valueOf(String.valueOf(delta)), ""+delta);
        dataList.add(data);

        mChartStates2 = findViewById(R.id.ChartProgressBarState2);

        mChartStates2.setDataList(dataList);
        mChartStates2.build();

    }

    void loadCropsBarChart(int rice, int sorghum, int maize, int cassava, int soybean) {
        ArrayList<BarData> dataList = new ArrayList<>();

        BarData data = new BarData("Rice ("+rice+")", Float.valueOf(String.valueOf(rice)), ""+rice);
        dataList.add(data);

        data = new BarData("Sorghum ("+sorghum+")", Float.valueOf(String.valueOf(sorghum)), ""+sorghum);
        dataList.add(data);

        data = new BarData("Maize ("+maize+")", Float.valueOf(String.valueOf(maize)), ""+maize);
        dataList.add(data);

        data = new BarData("Cassava ("+cassava+")", Float.valueOf(String.valueOf(cassava)), ""+cassava);
        dataList.add(data);

        data = new BarData("Soybean ("+soybean+")", Float.valueOf(String.valueOf(soybean)), ""+soybean);
        dataList.add(data);


        cropChart = findViewById(R.id.ChartProgressBarCrop);

        cropChart.setDataList(dataList);
        cropChart.build();
    }
    void loadPieChart(double withSmartPhone, double withoutSmartPhone, double canSpeakEnglish, double illitrates, double male, double female) {

        double percentageWithSmartPhone = (withSmartPhone / (withSmartPhone + withoutSmartPhone)) * 100;
        double percentageWithoutSmartPhone =  (withoutSmartPhone / (withSmartPhone + withoutSmartPhone)) * 100;

        double percentageCanSpeakEnglish = (canSpeakEnglish / (canSpeakEnglish + illitrates)) * 100;
        double percentageIllitrates = (illitrates / (canSpeakEnglish + illitrates)) * 100;

        double percentageMale = (male / (male + female)) * 100;
        double percentageFemale = (female / (male + female)) * 100;



        AnimatedPieView mAnimatedPieView = findViewById(R.id.animatedPieView);
        AnimatedPieViewConfig config = new AnimatedPieViewConfig();
        config.startAngle(-90)
                .drawText(true)
                .textSize(30)
                .addData(new SimplePieInfo((float) withSmartPhone,new PepperSingleColor.Builder().setColor(PepperColors.PURPLE700).build(), "With smartphone("+String.valueOf(percentageWithSmartPhone).substring(0,2)+"%)"))//数据（实现IPieInfo接口的bean）
                .addData(new SimplePieInfo((float) withoutSmartPhone, new PepperSingleColor.Builder().setColor(PepperColors.AMBER700).build(), "No smartphone("+String.valueOf(percentageWithoutSmartPhone).substring(0,2)+"%)"))
                .duration(2000);

        mAnimatedPieView.applyConfig(config);
        mAnimatedPieView.start();

        AnimatedPieView mAnimatedPieView2 = findViewById(R.id.animatedPieView2);
        AnimatedPieViewConfig config2 = new AnimatedPieViewConfig();
        config2.startAngle(-90)
                .drawText(true)
                .textSize(30)
                .addData(new SimplePieInfo((float) illitrates, Color.BLUE, "Illitrate("+String.valueOf(percentageIllitrates).substring(0,2)+"%)"))//数据（实现IPieInfo接口的bean）
                .addData(new SimplePieInfo((float) canSpeakEnglish, new PepperSingleColor.Builder().setColor(PepperColors.BROWN700).build(), "Can speak("+String.valueOf(percentageCanSpeakEnglish).substring(0,2)+"%)"))
                .duration(2000);

        mAnimatedPieView2.applyConfig(config2);
        mAnimatedPieView2.start();

        AnimatedPieView mAnimatedPieView3 = findViewById(R.id.animatedPieView3);
        AnimatedPieViewConfig config3 = new AnimatedPieViewConfig();
        config3.startAngle(-90)
                .drawText(true)
                .textSize(30)
                .addData(new SimplePieInfo((float) female, Color.RED, "Female("+String.valueOf(percentageFemale).substring(0,2)+"%)"))//数据（实现IPieInfo接口的bean）
                .addData(new SimplePieInfo((float) male, new PepperSingleColor.Builder().setColor(PepperColors.GREEN700).build(), "Male("+String.valueOf(percentageMale).substring(0,2)+"%)"))
                .duration(2000);

        mAnimatedPieView3.applyConfig(config3);
        mAnimatedPieView3.start();

    }

    void doClickers(final ArrayList<String> listOfFarmGroups, final List<FullFarmerPojo> fullFarmerPojoList) {
        findViewById(R.id.seeFarmGroupCard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentGroups = new Intent(DashboardActivity.this, FarmGroupActivity.class);
                intentGroups.putExtra("data",listOfFarmGroups);
                startActivity(intentGroups);
            }
        });

        findViewById(R.id.seeAgentsCard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(DashboardActivity.this, ListOfAgents.class));
            }
        });

    }
    void doGroupClick(final int sokotoFarmers, final int gombe, final int niger, final int ogun, final int jigawa, final int edo, final int kaduna, final int kebbi, final int delta, final int taraba){
        findViewById(R.id.seeStatesCard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashboardActivity.this, ByStatesActivity.class);
                intent.putExtra("sokoto", sokotoFarmers);
                intent.putExtra("gombe", gombe);
                intent.putExtra("niger", niger);
                intent.putExtra("ogun", ogun);
                intent.putExtra("jigawa", jigawa);
                intent.putExtra("edo", edo);
                intent.putExtra("Kaduna", kaduna);
                intent.putExtra("kebbi", kebbi);
                intent.putExtra("delta", delta);
                intent.putExtra("taraba", taraba);
                startActivity(intent);
            }
        });
    }

    @Override
    public void bankUpNow(int status) {

    }
}

