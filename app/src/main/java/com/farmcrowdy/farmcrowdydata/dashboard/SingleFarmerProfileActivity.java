package com.farmcrowdy.farmcrowdydata.dashboard;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.addFarmer.FullFarmerPojo;

public class SingleFarmerProfileActivity extends AppCompatActivity{
    FullFarmerPojo farmerPojo;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_farmer_info);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        farmerPojo = getIntent().getExtras().getParcelable("data");

        TextView textView = findViewById(R.id.dumpInfo);
        textView.setText(String.valueOf("Name: "+farmerPojo.getFname()+
                " \nPhone number: "+farmerPojo.getPhone()+
        "\n\nDate Of Birth: "+farmerPojo.getDob()+
        "\nMartial status: "+farmerPojo.getMarital_status()+
        "\nGender: "+farmerPojo.getGender()+
        "\nNumber of dependants: "+farmerPojo.getNumber_of_dependants()+
        "\n\nNext of kin: "+farmerPojo.getNextOfKin()+
        "\nNext of Kin Phone: "+farmerPojo.getNextOfKinPhone()+
        "\nCrop type: "+farmerPojo.getCrop_proficiency()+
        "\nLocal Government: "+farmerPojo.getGovtString()+
        "\n\nBVN: "+farmerPojo.getBvnNumber()+
        "\nBank Name: "+farmerPojo.getBankName()+
        "\nAccount no: "+farmerPojo.getAcct_number()+
        "\n\nMethod of ID: "+farmerPojo.getIdType()+
        "\nID Number: "+farmerPojo.getIdNumber()+
        "\nRole: "+farmerPojo.getRole()+
        "\n\nYears of Experience: "+farmerPojo.getYears_of_experience()+" years"+
        "\nIncome range: "+ farmerPojo.getIncome_range()+
        "\nFarm Land area: "+farmerPojo.getLand_area_farmed()+" hectares"+
        "\n\nPicture link: "+farmerPojo.getPro_image()+
        "\nCoordinates: Lat - "+farmerPojo.getLat1() +", Long - "+farmerPojo.getLong1()));
    }
}
