package com.farmcrowdy.farmcrowdydata.dashboard;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdydata.ApplicationInstance;
import com.farmcrowdy.farmcrowdydata.DirectPojo.FarmerPojo;
import com.farmcrowdy.farmcrowdydata.ListPojo.FarmerListPojo;
import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.ViewOfflineFarmers;
import com.farmcrowdy.farmcrowdydata.addFarmer.FullFarmerPojo;

import java.util.ArrayList;
import java.util.List;

public class FarmGroupActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    ArrayList<String> farmGroupList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_offline_farmers);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        farmGroupList = getIntent().getStringArrayListExtra("data");



        recyclerView = findViewById(R.id.offlineFarmersRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new FarmGroupRecyclerAdapter());

    }


    class FarmGroupRecyclerAdapter extends RecyclerView.Adapter<FarmGroupRecyclerAdapter.FarmGroupViewHolder> {

        @NonNull
        @Override
        public FarmGroupViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.view_groups_items, viewGroup, false);
            return new FarmGroupViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull FarmGroupViewHolder farmGroupViewHolder, int i) {
            farmGroupViewHolder.groupName.setText(farmGroupList.get(i));
            final ArrayList<FullFarmerPojo> farmersList = new ArrayList<>();
            for(FullFarmerPojo pojo: ApplicationInstance.getFullFarmerPojoList()) {
                if(pojo.getFarm_group_id().equals(farmGroupList.get(i))) farmersList.add(pojo);
            }

            farmGroupViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(FarmGroupActivity.this, ListOfAllFamersBasedOnParams.class);
                    intent.putExtra("data", farmersList);
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            if(farmGroupList==null) return 0;
            else return farmGroupList.size();
        }

        class FarmGroupViewHolder extends RecyclerView.ViewHolder {

            TextView groupName;
            public FarmGroupViewHolder(@NonNull View itemView) {
                super(itemView);
                groupName = itemView.findViewById(R.id.viewGroup_name);
            }
        }
    }
}
