package com.farmcrowdy.farmcrowdydata.dashboard;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.farmcrowdy.farmcrowdydata.ApplicationInstance;
import com.farmcrowdy.farmcrowdydata.MainActivity;
import com.farmcrowdy.farmcrowdydata.MyEndpoint;
import com.farmcrowdy.farmcrowdydata.addFarmer.FullFarmerPojo;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline_Table;
import com.farmcrowdy.farmcrowdydata.engine.LoadEffecient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.observers.DisposableObserver;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BackUpPresenterImpl implements BankUpPresenter {
private BackUpInterface backUpInterface;

    public BackUpPresenterImpl(BackUpInterface backUpInterface) {
        this.backUpInterface = backUpInterface;
    }
    private int counter = 0;

    @SuppressLint("CheckResult")
    @Override
    public void startWork() {
        Observable<Integer> startBackup = Observable.create(this::backUpToParentDB);

        startBackup.subscribeWith(new DisposableObserver<Integer>() {
            @Override
            public void onNext(Integer integer) {
               backUpInterface.bankUpNow(integer);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

   

    ArrayList<String> filteredList = new ArrayList<>();
    ArrayList<FullFarmerPojo> realPojo = new ArrayList<>();
    ArrayList<FullFarmerPojo> duplicatesPojo = new ArrayList<>();
    private void backUpToParentDB(ObservableEmitter<Integer> emitter) {

        ApplicationInstance.getFirestore().collection("novusData").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(!task.isSuccessful()) {
                    emitter.onNext(5);
                    emitter.onComplete();
                    return;
                }

                List<FullFarmerPojo> fullFarmerPojoList = task.getResult().toObjects(FullFarmerPojo.class);

                Log.d("CHECKER", "full size is "+fullFarmerPojoList.size());
                for(int i = 0; i<fullFarmerPojoList.size(); i++ )  {

                    if(!filteredList.contains(fullFarmerPojoList.get(i).getFarmerId())) {
                        filteredList.add(fullFarmerPojoList.get(i).getFarmerId());
                        realPojo.add(fullFarmerPojoList.get(i));
                    }
                    else {
                        duplicatesPojo.add(fullFarmerPojoList.get(i));
                    }
                }


                Log.d("CHECKER", "Filtered size is: "+filteredList.size());
                doUploadToAzure(emitter, realPojo, 1);
                Log.d("CHECKER", "Filtered data completed");
            }
        });
    }

    @Override
    public void doSynced() {
        final List<StoreProfileOffline> myStore = SQLite.select().from(StoreProfileOffline.class).where(StoreProfileOffline_Table.synced.is(false)).queryList();
        for (final StoreProfileOffline profile : myStore) {
            Bitmap high;
            counter = counter + 1;

            FullFarmerPojo pojo = new FullFarmerPojo();
            pojo.setRole(profile.getRole());
            pojo.setReadWriteEnglish(profile.isReadWriteEnglish());
            pojo.setHaveSmartPhone(profile.isHaveSmartPhone());
            pojo.setUser_add_id(profile.getUser_add_id());
            pojo.setGovtString(profile.getGovtString());
            pojo.setYears_of_experience(profile.getYears_of_experience());
            pojo.setIncome_range(profile.getIncome_range());
            pojo.setIdNumber(profile.getIdNumber());
            pojo.setIdType(profile.getIdType());
            pojo.setLand_area_farmed(profile.getLand_area_farmed());
            pojo.setBvnNumber(profile.getBvnNumber());
            pojo.setAcct_number(profile.getAcct_number());
            pojo.setBankName(profile.getBankName());
            pojo.setBank_id(profile.getBank_id());
            pojo.setFname(profile.getFname());
            pojo.setPhone(profile.getPhone());
            pojo.setState_id(profile.getState_id());
            pojo.setDob(profile.getDob());
            pojo.setFprint(profile.getF_print());
            pojo.setPro_image(profile.getPro_image());
            pojo.setMarital_status(profile.getMarital_status());
            pojo.setLocal_id(profile.getLocal_id());
            pojo.setGender(profile.getGender());
            pojo.setPro_image_thumbnail(profile.getPro_image_thumbnail());

            pojo.setNumber_of_dependants(profile.getNumber_of_dependants());
            pojo.setFarm_group_id(profile.getFarm_group_id());
            pojo.setCrop_proficiency(profile.getCrop_proficiency());
            pojo.setFarmerId(profile.getFarmerId());

            pojo.setLat1(profile.getLat1());
            pojo.setLat2(profile.getLat2());
            pojo.setLat3(profile.getLat3());
            pojo.setLat4(profile.getLat4());

            pojo.setLong1(profile.getLong1());
            pojo.setLong2(profile.getLong2());
            pojo.setLong3(profile.getLong3());
            pojo.setLong4(profile.getLong4());

            pojo.setUploadedToParent(false);
            pojo.setNextOfKin(profile.getNextOfKin());
            pojo.setNextOfKinPhone(profile.getNextOfKinPhone());
            pojo.setTimeStamp(profile.getTimeStamp());

            try {
                String ts = "error";
                String tsIntro = "error";
                if (new File(profile.getPro_image()).exists()) {
                    high = new LoadEffecient().decodeSampledBitmapFromFile(profile.getPro_image(), LoadEffecient.maxHeight, LoadEffecient.maxWidth);
                    File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
                    ts = getPhotoUrl(file);
                }

                if (new File(profile.getPro_image_thumbnail()).exists()) {
                    high = new LoadEffecient().decodeSampledBitmapFromFile(profile.getPro_image_thumbnail(), LoadEffecient.maxHeight, LoadEffecient.maxWidth);
                    File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
                    tsIntro = getPhotoIdentification(file);
                }

               pojo.setPro_image(ts);
                pojo.setPro_image_thumbnail(tsIntro);
            } catch (Exception e) { pojo.setPro_image("error"); pojo.setPro_image_thumbnail("error");}

            MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
            RequestBody fnameReq =  RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getFname()));
            RequestBody roleReq =  RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getRole()));
            RequestBody Lat1Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLat1()) ? "0.00" : pojo.getLat1() );
            RequestBody Lat2Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLat2()) ?  "0.00" :pojo.getLat2() );
            RequestBody Lat3Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLat3()) ?  "0.00" :pojo.getLat3());
            RequestBody Lat4Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLat4()) ?  "0.00" :pojo.getLat4());
            RequestBody Long1Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLong1()) ?  "0.00" :pojo.getLong1() );
            RequestBody Long2Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLong2()) ?  "0.00" :pojo.getLong2());
            RequestBody Long3Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLong3()) ?  "0.00" :pojo.getLong3() );
            RequestBody Long4Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLong4()) ?  "0.00" :pojo.getLong4() );
            RequestBody farmerIdReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getFarmerId()));
            RequestBody timeStampReq = RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getTimeStamp()) ?  "0" :pojo.getTimeStamp());
            RequestBody govtStringReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getGovtString()));
            RequestBody farm_group_idReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getFarm_group_id()));
            RequestBody bankNameReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getBankName()));
            RequestBody bvnNumberReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getBvnNumber()));
            RequestBody idTypeReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getIdType()));
            RequestBody idNumberReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getIdNumber()));
            RequestBody dobReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getDob()));
            RequestBody genderReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getGender()));
            RequestBody phoneReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getPhone()));
            RequestBody marital_statusReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getMarital_status()));
            RequestBody crop_proficiencyReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getCrop_proficiency()));
            RequestBody income_rangeReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getIncome_range()));
            RequestBody pro_imageReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getPro_image()));
            RequestBody pro_imageIdentificationReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getPro_image_thumbnail()));

            RequestBody fPrintReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getFprint()));


            RequestBody acct_numberReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getAcct_number()));
            RequestBody land_area_farmedReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getLand_area_farmed()));
            RequestBody nextOfKinReq = RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getNextOfKin()) ?  "empty" :pojo.getNextOfKin());
            RequestBody nextOfKinPhoneReq = RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getNextOfKinPhone()) ?  "0" :pojo.getNextOfKinPhone());
            RequestBody uploadedToParentReq = RequestBody.create(MediaType.parse("text/plain"), checkForNull(String.valueOf(pojo.isUploadedToParent())) ?  "false" :String.valueOf(pojo.isUploadedToParent()) );
            RequestBody readWriteEnglishReq = RequestBody.create(MediaType.parse("text/plain"), checkForNull(String.valueOf(pojo.isReadWriteEnglish())) ?  "false" :String.valueOf(pojo.isReadWriteEnglish()));
            RequestBody haveSmartPhoneReq = RequestBody.create(MediaType.parse("text/plain"), checkForNull(String.valueOf(pojo.isHaveSmartPhone())) ?  "false" :String.valueOf(pojo.isHaveSmartPhone()));


            Call<ResponseBody> caller = myEndpoint.publicFarmers(
                    fnameReq,
                    roleReq,Lat1Req, Lat2Req, Lat3Req, Lat4Req, Long1Req, Long2Req, Long3Req, Long4Req, farmerIdReq,
                    timeStampReq, govtStringReq, farm_group_idReq,bankNameReq, bvnNumberReq, idTypeReq,idNumberReq,dobReq,
                    genderReq,phoneReq,marital_statusReq,crop_proficiencyReq,income_rangeReq,pro_imageReq, pro_imageIdentificationReq, acct_numberReq,land_area_farmedReq,
                    nextOfKinReq, nextOfKinPhoneReq, pojo.getUser_add_id(), pojo.getState_id(), pojo.getLocal_id(), pojo.getFarm_location_id(),
                    pojo.getBank_id(), pojo.getNumber_of_dependants(), pojo.getYears_of_experience(), uploadedToParentReq,
                    readWriteEnglishReq, haveSmartPhoneReq, 2, fPrintReq);

            caller.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful()) {

                            profile.setSynced(true);
                            profile.update();


                    }
                    else {
                        backUpInterface.bankUpNow(2);
                        Log.d("DASHER", "error is "+response.message());
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.d("DASHER", "error is two "+t.getMessage());
                    backUpInterface.bankUpNow(2);
                    return;
                }
            });
        }
        backUpInterface.bankUpNow(1);

    }
    
    private void doUploadToAzure(ObservableEmitter<Integer> emitter, List<FullFarmerPojo> realPojo, int type) {
        
        for(FullFarmerPojo pojo: realPojo) {
            Log.d("CHECKER", "total size is: "+pojo.getFarmerId());

            counter = counter + 1;
            MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);
            RequestBody fnameReq =  RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getFname()));
            RequestBody roleReq =  RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getRole()));
            RequestBody Lat1Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLat1()) ? "0.00" : pojo.getLat1() );
            RequestBody Lat2Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLat2()) ?  "0.00" :pojo.getLat2() );
            RequestBody Lat3Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLat3()) ?  "0.00" :pojo.getLat3());
            RequestBody Lat4Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLat4()) ?  "0.00" :pojo.getLat4());
            RequestBody Long1Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLong1()) ?  "0.00" :pojo.getLong1() );
            RequestBody Long2Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLong2()) ?  "0.00" :pojo.getLong2());
            RequestBody Long3Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLong3()) ?  "0.00" :pojo.getLong3() );
            RequestBody Long4Req =  RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getLong4()) ?  "0.00" :pojo.getLong4() );
            RequestBody farmerIdReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getFarmerId()));
            RequestBody timeStampReq = RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getTimeStamp()) ?  "0" :pojo.getTimeStamp());
            RequestBody govtStringReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getGovtString()));
            RequestBody farm_group_idReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getFarm_group_id()));
            RequestBody bankNameReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getBankName()));
            RequestBody bvnNumberReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getBvnNumber()));
            RequestBody idTypeReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getIdType()));
            RequestBody idNumberReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getIdNumber()));
            RequestBody dobReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getDob()));
            RequestBody genderReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getGender()));
            RequestBody phoneReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getPhone()));
            RequestBody marital_statusReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getMarital_status()));
            RequestBody crop_proficiencyReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getCrop_proficiency()));
            RequestBody income_rangeReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getIncome_range()));
            RequestBody pro_imageReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getPro_image()));
            RequestBody acct_numberReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getAcct_number()));
            RequestBody land_area_farmedReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getLand_area_farmed()));
            RequestBody nextOfKinReq = RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getNextOfKin()) ?  "empty" :pojo.getNextOfKin());
            RequestBody nextOfKinPhoneReq = RequestBody.create(MediaType.parse("text/plain"), checkForNull(pojo.getNextOfKinPhone()) ?  "0" :pojo.getNextOfKinPhone());
            RequestBody uploadedToParentReq = RequestBody.create(MediaType.parse("text/plain"), checkForNull(String.valueOf(pojo.isUploadedToParent())) ?  "false" :String.valueOf(pojo.isUploadedToParent()) );
            RequestBody readWriteEnglishReq = RequestBody.create(MediaType.parse("text/plain"), checkForNull(String.valueOf(pojo.isReadWriteEnglish())) ?  "false" :String.valueOf(pojo.isReadWriteEnglish()));
            RequestBody haveSmartPhoneReq = RequestBody.create(MediaType.parse("text/plain"), checkForNull(String.valueOf(pojo.isHaveSmartPhone())) ?  "false" :String.valueOf(pojo.isHaveSmartPhone()));
            RequestBody pro_imageIdentificationReq = RequestBody.create(MediaType.parse("text/plain"), stripNull(pojo.getPro_image_thumbnail()));


            Call<ResponseBody> caller = myEndpoint.publicFarmers(
                    fnameReq,
                    roleReq,Lat1Req, Lat2Req, Lat3Req, Lat4Req, Long1Req, Long2Req, Long3Req, Long4Req, farmerIdReq,
                    timeStampReq, govtStringReq, farm_group_idReq,bankNameReq, bvnNumberReq, idTypeReq,idNumberReq,dobReq,
                    genderReq,phoneReq,marital_statusReq,crop_proficiencyReq,income_rangeReq,pro_imageReq,pro_imageIdentificationReq, acct_numberReq,land_area_farmedReq,
                    nextOfKinReq, nextOfKinPhoneReq, pojo.getUser_add_id(), pojo.getState_id(), pojo.getLocal_id(), pojo.getFarm_location_id(),
                    pojo.getBank_id(), pojo.getNumber_of_dependants(), pojo.getYears_of_experience(), uploadedToParentReq,
                    readWriteEnglishReq, haveSmartPhoneReq, 2, null);

            caller.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    emitter.onNext(2);
                    emitter.onComplete();
                }
            });




        }
    }
    private  boolean checkForNull(String entry) {
        return entry == null || entry.isEmpty();
    }
    private  String stripNull(String entry) {
        if(entry == null || entry.isEmpty()) {
            return "empty";
        }
        else return entry;
    }
    private String getPhotoUrl(File file) {
        StorageReference storageRef = FirebaseStorage.getInstance().getReference();
        int photoRan = new Random().nextInt(100);
        int photoRumm = new Random().nextInt(999999);
        String photoName = "fcdy"+photoRan+photoRumm+new Date().getTime();
        final StorageReference mountainsRef = storageRef.child("novus/"+photoName+".jpg");
        UploadTask uploadTask = mountainsRef.putFile(Uri.fromFile(file));

        if(uploadTask.isSuccessful()) {
            return  mountainsRef.getDownloadUrl().toString();
        }
        else {
            return photoName;
        }

    }
    private String getPhotoIdentification(File file) {
        StorageReference storageRef = FirebaseStorage.getInstance().getReference();
        int photoRan = new Random().nextInt(100);
        int photoRumm = new Random().nextInt(999999);
        String photoName = "fcdy"+photoRan+photoRumm+new Date().getTime();
        final StorageReference mountainsRef = storageRef.child("novusIdentityPhotos/"+photoName+".jpg");
        UploadTask uploadTask = mountainsRef.putFile(Uri.fromFile(file));

            return photoName;

    }
}
