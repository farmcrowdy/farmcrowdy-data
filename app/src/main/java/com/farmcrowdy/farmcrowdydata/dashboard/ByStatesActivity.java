package com.farmcrowdy.farmcrowdydata.dashboard;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.farmcrowdy.farmcrowdydata.R;

public class ByStatesActivity extends AppCompatActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.states_layout);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.state1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ByStatesActivity.this, PieChartByStates.class);
                intent.putExtra("data", 34);
                intent.putExtra("stateName", "Sokoto");
                intent.putExtra("num", getIntent().getExtras().getInt("sokoto", 1));
                startActivity(intent);
            }
        });

        findViewById(R.id.state2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ByStatesActivity.this, PieChartByStates.class);
                intent.putExtra("data", 16);
                intent.putExtra("stateName", "Gombe");
                intent.putExtra("num", getIntent().getExtras().getInt("gombe", 1));
                startActivity(intent);
            }
        });

        findViewById(R.id.state3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ByStatesActivity.this, PieChartByStates.class);
                intent.putExtra("data", 27);
                intent.putExtra("stateName", "Niger");
                intent.putExtra("num", getIntent().getExtras().getInt("niger", 1));
                startActivity(intent);
            }
        });

        findViewById(R.id.state4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ByStatesActivity.this, PieChartByStates.class);
                intent.putExtra("data", 28);
                intent.putExtra("stateName", "Ogun");
                intent.putExtra("num", getIntent().getExtras().getInt("ogun", 1));
                startActivity(intent);
            }
        });

        findViewById(R.id.state5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ByStatesActivity.this, PieChartByStates.class);
                intent.putExtra("data", 18);
                intent.putExtra("stateName", "Jigawa");
                intent.putExtra("num", getIntent().getExtras().getInt("jigawa", 1));
                startActivity(intent);
            }
        });

        findViewById(R.id.state6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ByStatesActivity.this, PieChartByStates.class);
                intent.putExtra("data", 12);
                intent.putExtra("stateName", "Edo");
                intent.putExtra("num", getIntent().getExtras().getInt("edo", 1));
                startActivity(intent);
            }
        });

        findViewById(R.id.state7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ByStatesActivity.this, PieChartByStates.class);
                intent.putExtra("data", 19);
                intent.putExtra("stateName", "Kaduna");
                intent.putExtra("num", getIntent().getExtras().getInt("Kaduna", 1));
                startActivity(intent);
            }
        });

        findViewById(R.id.state8).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ByStatesActivity.this, PieChartByStates.class);
                intent.putExtra("data", 22);
                intent.putExtra("stateName", "Kebbi");
                intent.putExtra("num", getIntent().getExtras().getInt("kebbi", 1));
                startActivity(intent);
            }
        });
        findViewById(R.id.state9).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ByStatesActivity.this, PieChartByStates.class);
                intent.putExtra("data", 10);
                intent.putExtra("stateName", "Delta");
                intent.putExtra("num", getIntent().getExtras().getInt("delta", 1));
                startActivity(intent);
            }
        });
        findViewById(R.id.state10).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ByStatesActivity.this, PieChartByStates.class);
                intent.putExtra("data", 10);
                intent.putExtra("stateName", "Taraba");
                intent.putExtra("num", getIntent().getExtras().getInt("taraba", 1));
                startActivity(intent);
            }
        });

    }
}
