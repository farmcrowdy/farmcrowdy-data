package com.farmcrowdy.farmcrowdydata.dashboard;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdydata.ApplicationInstance;
import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.addFarmer.FullFarmerPojo;
import com.razerdp.widget.animatedpieview.AnimatedPieView;
import com.razerdp.widget.animatedpieview.AnimatedPieViewConfig;
import com.razerdp.widget.animatedpieview.data.SimplePieInfo;
import com.tobilvr.works.peppercolor.PepperColors;
import com.tobilvr.works.peppercolor.PepperSingleColor;

import java.util.ArrayList;

public class PieChartByStates  extends AppCompatActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.piechart_state);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        String stateName = getIntent().getExtras().getString("stateName");
        toolbar.setTitle("Farmers in "+stateName);
        toolbar.setTitleMarginTop(10);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView pieTotal = findViewById(R.id.pie_total);
        int numberOfFarmersInTheState = +getIntent().getExtras().getInt("num", 0);
        pieTotal.setText(String.valueOf("There are "+numberOfFarmersInTheState+" farmers in "+stateName+" state"));
        final int chosenState = getIntent().getExtras().getInt("data", 1);
        findViewById(R.id.pie_seeLocal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PieChartByStates.this, SeeListOfLocalGovernment.class);
                intent.putExtra("data", chosenState);
                startActivity(intent);
            }
        });


        Log.d("Dasher", "Chosen state is: "+chosenState);
        ArrayList<FullFarmerPojo> myLister = new ArrayList<>();
        for(FullFarmerPojo pojo: ApplicationInstance.getFullFarmerPojoList()) {
            if(pojo.getState_id() == chosenState) myLister.add(pojo);
        }

        int withSmartPhone = 0;
        int canReadEnglish = 0;
        int isMale = 0;

        for (FullFarmerPojo fullFarmerPojo : myLister) {
            if (fullFarmerPojo.isHaveSmartPhone()) withSmartPhone = withSmartPhone + 1;
            if (fullFarmerPojo.isReadWriteEnglish()) canReadEnglish = canReadEnglish + 1;
            if (fullFarmerPojo.getGender().equals("Male")) isMale = isMale + 1;
        }
        int withoutSmartPhone = myLister.size()  - withSmartPhone;
        int illitrate = myLister.size() - canReadEnglish;
        int isFemale = myLister.size() - isMale;

        Log.d("Dasher", "people with smartPhone are: "+ withSmartPhone);
        loadPieChart(withSmartPhone, withoutSmartPhone, canReadEnglish, illitrate,isMale, isFemale);



    }
    void loadPieChart(double withSmartPhone, double withoutSmartPhone, double canSpeakEnglish, double illitrates, double male, double female) {

        double percentageWithSmartPhone = (withSmartPhone / (withSmartPhone + withoutSmartPhone)) * 100;
        double percentageWithoutSmartPhone =  (withoutSmartPhone / (withSmartPhone + withoutSmartPhone)) * 100;

        double percentageCanSpeakEnglish = (canSpeakEnglish / (canSpeakEnglish + illitrates)) * 100;
        double percentageIllitrates = (illitrates / (canSpeakEnglish + illitrates)) * 100;

        double percentageMale = (male / (male + female)) * 100;
        double percentageFemale = (female / (male + female)) * 100;



        AnimatedPieView mAnimatedPieView = findViewById(R.id.animatedPieView);
        AnimatedPieViewConfig config = new AnimatedPieViewConfig();
        config.startAngle(-90)
                .drawText(true)
                .textSize(30)
                .addData(new SimplePieInfo((float) withSmartPhone,new PepperSingleColor.Builder().setColor(PepperColors.PURPLE700).build(), "With smartphone("+String.valueOf(percentageWithSmartPhone).substring(0,3)+"%)"))//数据（实现IPieInfo接口的bean）
                .addData(new SimplePieInfo((float) withoutSmartPhone, new PepperSingleColor.Builder().setColor(PepperColors.AMBER700).build(), "No smartphone("+String.valueOf(percentageWithoutSmartPhone).substring(0,3)+"%)"))
                .duration(2000);

        mAnimatedPieView.applyConfig(config);
        mAnimatedPieView.start();

        AnimatedPieView mAnimatedPieView2 = findViewById(R.id.animatedPieView2);
        AnimatedPieViewConfig config2 = new AnimatedPieViewConfig();
        config2.startAngle(-90)
                .drawText(true)
                .textSize(30)
                .addData(new SimplePieInfo((float) illitrates, Color.BLUE, "Illitrate("+String.valueOf(percentageIllitrates).substring(0,3)+"%)"))//数据（实现IPieInfo接口的bean）
                .addData(new SimplePieInfo((float) canSpeakEnglish, new PepperSingleColor.Builder().setColor(PepperColors.BROWN700).build(), "Can speak("+String.valueOf(percentageCanSpeakEnglish).substring(0,3)+"%)"))
                .duration(2000);

        mAnimatedPieView2.applyConfig(config2);
        mAnimatedPieView2.start();

        AnimatedPieView mAnimatedPieView3 = findViewById(R.id.animatedPieView3);
        AnimatedPieViewConfig config3 = new AnimatedPieViewConfig();
        config3.startAngle(-90)
                .drawText(true)
                .textSize(30)
                .addData(new SimplePieInfo((float) female, Color.RED, "Female("+String.valueOf(percentageFemale).substring(0,3)+"%"))//数据（实现IPieInfo接口的bean）
                .addData(new SimplePieInfo((float) male, new PepperSingleColor.Builder().setColor(PepperColors.GREEN700).build(), "Male("+String.valueOf(percentageMale).substring(0,3)+"%)"))
                .duration(2000);

        mAnimatedPieView3.applyConfig(config3);
        mAnimatedPieView3.start();

    }
}
