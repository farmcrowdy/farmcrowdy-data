package com.farmcrowdy.farmcrowdydata.addFarmer;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdydata.DirectPojo.BanksJojo;
import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.engine.LoadEffecient;
import com.rafakob.floatingedittext.FloatingEditText;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class BankAndIdentificationActivity extends AppCompatActivity {
    int idExpectedLength = 1;
    Spinner listOfBanks, typeOfIdentification, roleSpinner;
    ArrayList<String> bankListString;
    ArrayList<Integer> bankListInt;
    int bank_selected;
    String methodIdString, bankName;
    Switch readWrite, smartPhone;
    String imageList="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_2);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        typeOfIdentification = findViewById(R.id.methodid_spinner);

        final FloatingEditText accountNumberEdit = findViewById(R.id.account_number_editText);
        final FloatingEditText bvnEdit = findViewById(R.id.bvn_editText);
        readWrite = findViewById(R.id.readWrite);
        smartPhone = findViewById(R.id.smartPhone);
        roleSpinner = findViewById(R.id.roleSpinner);
        final FloatingEditText methodIdEdit = findViewById(R.id.method_id_editText);
        final FullFarmerPojo fullFarmerPojo = getIntent().getExtras().getParcelable("data");
        getBankAdding();
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(accountNumberEdit.getText().length() > 8) {
                    if(bvnEdit.getText().length() == 11) {
                        if(methodIdEdit.getText().length() == idExpectedLength) {
                            if(!bankName.contains("Select")) {
                                fullFarmerPojo.setBank_id(bank_selected);
                                fullFarmerPojo.setAcct_number(accountNumberEdit.getText());
                                fullFarmerPojo.setBankName(bankName);
                                fullFarmerPojo.setBvnNumber(bvnEdit.getText());
                                fullFarmerPojo.setIdType(methodIdString);
                                fullFarmerPojo.setIdNumber(methodIdEdit.getText());
                                fullFarmerPojo.setHaveSmartPhone(smartPhone.isChecked());
                                fullFarmerPojo.setReadWriteEnglish(readWrite.isChecked());
                                fullFarmerPojo.setPro_image_thumbnail(imageList);
                                fullFarmerPojo.setRole(roleSpinner.getSelectedItem().toString());


                                Intent intent = new Intent(BankAndIdentificationActivity.this, FarmingHistoryActivity.class);
                                intent.putExtra("data", fullFarmerPojo);
                                startActivity(intent);
                            }
                            else {
                                Toast.makeText(BankAndIdentificationActivity.this, "Choose a real bank from the list "+idExpectedLength+" chars are expected", Toast.LENGTH_SHORT).show();

                            }


                        }
                        else {
                            Toast.makeText(BankAndIdentificationActivity.this, "Method of identification number not complete: "+idExpectedLength+" chars are expected", Toast.LENGTH_SHORT).show();

                        }
                    }
                    else Toast.makeText(BankAndIdentificationActivity.this, "BVN number not complete 11 chars are expected", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(BankAndIdentificationActivity.this, "Account number incorrect", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.identityPicture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(BankAndIdentificationActivity.this);
            }
        });
    }

    void getBankAdding() {
        listOfBanks = findViewById(R.id.banks_spinner);
        List<BanksJojo> groupList = SQLite.select().
                from(BanksJojo.class).queryList();

        bankListString = new ArrayList<>();
        bankListInt = new ArrayList<>();

        for(int i=0; i<groupList.size(); i++){
            bankListString.add(groupList.get(i).getBank_name());
            bankListInt.add(groupList.get(i).getBank_id());
        }
        ArrayAdapter<String> dataAdapterState = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, bankListString);
        dataAdapterState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listOfBanks.setAdapter(dataAdapterState);
        listOfBanks.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                bank_selected = bankListInt.get(i);
                bankName = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        typeOfIdentification.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                methodIdString = adapterView.getItemAtPosition(i).toString();
                switch (i) {
                    case 0: idExpectedLength = 19;
                    break;
                    case 1: idExpectedLength = 12;
                    break;
                    case 2: idExpectedLength = 9;
                    break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imageList = resultUri.getPath();
                if (!imageList.equals("empty")) {
                    Bitmap high;
                    try {
                        high = new LoadEffecient().decodeSampledBitmapFromFile(imageList, 200, 200);
                        File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
                        ImageView circleImageView = findViewById(R.id.identityPicture);
                        circleImageView.setImageURI(Uri.parse(file.getPath()));

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

}
