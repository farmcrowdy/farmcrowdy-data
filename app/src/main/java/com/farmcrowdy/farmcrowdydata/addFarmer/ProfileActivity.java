package com.farmcrowdy.farmcrowdydata.addFarmer;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdydata.ApplicationInstance;
import com.farmcrowdy.farmcrowdydata.DirectPojo.CropPojo;
import com.farmcrowdy.farmcrowdydata.DirectPojo.FarmLocationPojo;
import com.farmcrowdy.farmcrowdydata.DirectPojo.LocalGovtPojo;
import com.farmcrowdy.farmcrowdydata.DirectPojo.LocalGovtPojo_Table;
import com.farmcrowdy.farmcrowdydata.DirectPojo.StatePojo;
import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.engine.LoadEffecient;
import com.farmcrowdy.farmcrowdydata.engine.TinyDB;
import com.rafakob.floatingedittext.FloatingEditText;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity {
    FloatingEditText firstnameEdit, phoneNumberEdit, noOfDependantsEdit, nextOfKin, nextOfKinPhone;
    Spinner genderspinner, maritalStatus, cropSpinner, locationSpinner,  stateSpinner, 
            localGovtSpinner, yearSpinner, monthSpinner, daySpinner, groupSpinner;
    

    String genderString, maritalString, cropString, govtString, stateString, groupString;
    int cropInt, stateInt, govtInt;

    ArrayList<String> cropListString = new ArrayList<>();
    ArrayList<String> farmgroupERList = new ArrayList<>();
    ArrayList<Integer> cropListInt = new ArrayList<>();
    
    ArrayList<String> stateListString = new ArrayList<>();
    ArrayList<Integer> stateListInt = new ArrayList<>();

    ArrayList<String> localGovtListString = new ArrayList<>();
    ArrayList<Integer> localGovtListInt = new ArrayList<>();
    private String mYear, mMonth, mDay;
    String imageList="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_1);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        firstnameEdit = findViewById(R.id.fullname_editText);
        phoneNumberEdit = findViewById(R.id.phone_editText);
        noOfDependantsEdit = findViewById(R.id.dependant_editText);
        genderspinner = findViewById(R.id.genderSpinner);
        cropSpinner = findViewById(R.id.proficiency_spinner);
        locationSpinner = findViewById(R.id.farm_location_spinner);
        stateSpinner = findViewById(R.id.state_spinner);
        maritalStatus = findViewById(R.id.spinnerMaritalStatus);
        localGovtSpinner = findViewById(R.id.LGA_spinner);
        yearSpinner = findViewById(R.id.yearSpinner);
        monthSpinner = findViewById(R.id.monthSpinner);
        daySpinner = findViewById(R.id.daySpinner);
        groupSpinner = findViewById(R.id.group_spinner);
        nextOfKin = findViewById(R.id.nextOfKin_editText);
        nextOfKinPhone = findViewById(R.id.nextOfKinPhone_editText);

        getAddings();
        setupAllSpinners();
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(firstnameEdit.getText().length() > 5) {
                    if(phoneNumberEdit.getText().trim().length() == 11) {
                        if(!mYear.equals("Year") || !mMonth.equals("Month") ||!mDay.equals("Day")) {
                            if(cropString !=null && govtString !=null && stateString !=null) {
                                if(groupString !=null) {
                                    if(!noOfDependantsEdit.getText().isEmpty()) {
                                        if(!imageList.isEmpty()) {
                                            if(!nextOfKin.getText().isEmpty()) {
                                                if(!nextOfKinPhone.getText().isEmpty()) {
                                                    FullFarmerPojo fullFarmerPojo = new FullFarmerPojo();
                                                    fullFarmerPojo.setFname(firstnameEdit.getText());
                                                    fullFarmerPojo.setGovtString(govtString);
                                                    fullFarmerPojo.setPhone(phoneNumberEdit.getText());
                                                    fullFarmerPojo.setNumber_of_dependants(Integer.valueOf(noOfDependantsEdit.getText()));
                                                    fullFarmerPojo.setDob(mDay+" "+mMonth+", "+mYear);
                                                    fullFarmerPojo.setGender(genderString);
                                                    fullFarmerPojo.setCrop_proficiency(cropString);
                                                    fullFarmerPojo.setFarm_group_id(groupString.toLowerCase());
                                                    fullFarmerPojo.setState_id(stateInt);
                                                    fullFarmerPojo.setMarital_status(maritalString);
                                                    fullFarmerPojo.setLocal_id(govtInt);
                                                    fullFarmerPojo.setUser_add_id(new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1));
                                                    fullFarmerPojo.setPro_image(imageList);
                                                    fullFarmerPojo.setNextOfKin(nextOfKin.getText());
                                                    fullFarmerPojo.setNextOfKinPhone(nextOfKinPhone.getText());

                                                    Intent intent = new Intent(ProfileActivity.this, BankAndIdentificationActivity.class);
                                                    intent.putExtra("data", fullFarmerPojo);
                                                    startActivity(intent);

                                                }else Toast.makeText(ProfileActivity.this, "Please, next of kin Phone number is required", Toast.LENGTH_SHORT).show();
                                            } else Toast.makeText(ProfileActivity.this, "Please, next of kin name is required", Toast.LENGTH_SHORT).show();
                                        }  else  Toast.makeText(ProfileActivity.this, "Please take or upload farmer's picture", Toast.LENGTH_SHORT).show();
                                    } else Toast.makeText(ProfileActivity.this, "Please provide Number of Dependant", Toast.LENGTH_SHORT).show();
                                } else Toast.makeText(ProfileActivity.this, "Farm group name too short", Toast.LENGTH_SHORT).show();
                            } else Toast.makeText(ProfileActivity.this, "Please select the crop type, state and local government", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ProfileActivity.this, "Please select date of birth", Toast.LENGTH_SHORT).show();
                        }
                    }
                   else Toast.makeText(ProfileActivity.this, "Phone number incorrect", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(ProfileActivity.this, "Full name too short", Toast.LENGTH_SHORT).show();
                }  
            }
        });
        
    }

    private void getAddings() {

        List<StoreFarmGroups> farmGroupList = SQLite.select().
                from(StoreFarmGroups.class).queryList();


        for(StoreFarmGroups group : farmGroupList){
            farmgroupERList.add(group.getGroupName());
        }


        List<CropPojo> groupList = SQLite.select().
                from(CropPojo.class).queryList();

        cropListString = new ArrayList<>();
        cropListInt = new ArrayList<>();

        for(int i=0; i<groupList.size(); i++){
            cropListString.add(groupList.get(i).getCrop_name());
            cropListInt.add(groupList.get(i).getCrop_id());
            Log.d("DO_TESTING", "this is an updated for number of crops"+cropListString.get(i));
        }



        List<StatePojo> stateList = SQLite.select().from(StatePojo.class).queryList();
        for(int i=0; i<stateList.size(); i++){
            stateListString.add(stateList.get(i).getName());
            stateListInt.add(stateList.get(i).getState_id());
            Log.d("DO_TESTING", "this is an updated for number of states"+stateListString.get(i));
        }

        findViewById(R.id.takeUploadPhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(ProfileActivity.this);
            }
        });


    }
    private void setupAllSpinners() {
        genderspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                genderString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        groupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                groupString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mYear = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mMonth = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        daySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mDay = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        maritalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                maritalString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<String> dataAdapterGroup = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, farmgroupERList);
        dataAdapterGroup.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupSpinner.setAdapter(dataAdapterGroup);


        ArrayAdapter<String> dataAdapterState = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, stateListString);
        dataAdapterState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stateSpinner.setAdapter(dataAdapterState);



        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stateInt = stateListInt.get(i);
                stateString = adapterView.getItemAtPosition(i).toString();
                if(i!=0) {

                    List<LocalGovtPojo> govtList = SQLite.select().from(LocalGovtPojo.class).where(LocalGovtPojo_Table.state_id.is(stateInt)).
                            queryList();
                    localGovtListString = new ArrayList<>();
                    for(int p=0; p<govtList.size(); p++) {
                        localGovtListString.add(govtList.get(p).getLocal_name());
                        localGovtListInt.add(govtList.get(p).getLocal_id());
                    }

                    ArrayAdapter<String> dataAdapterGovt = new ArrayAdapter<String>(ProfileActivity.this,
                            android.R.layout.simple_spinner_item, localGovtListString);
                    dataAdapterGovt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    localGovtSpinner.setAdapter(dataAdapterGovt);

                    localGovtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            govtInt = localGovtListInt.get(i);
                            govtString = adapterView.getItemAtPosition(i).toString();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        cropSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i!=0) {
                    //cropInt = cropListInt.get(i);
                    cropString = adapterView.getItemAtPosition(i).toString();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imageList = resultUri.getPath();
                if (!imageList.equals("empty")) {
                    Bitmap high;
                    try {
                        high = new LoadEffecient().decodeSampledBitmapFromFile(imageList, 200, 200);
                        File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
                        CircleImageView circleImageView = findViewById(R.id.selected_photo);
                        circleImageView.setImageURI(Uri.parse(file.getPath()));

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
