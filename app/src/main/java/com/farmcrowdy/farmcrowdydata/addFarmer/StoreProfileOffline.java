package com.farmcrowdy.farmcrowdydata.addFarmer;

import com.farmcrowdy.farmcrowdydata.engine.MyDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

@Table(database = MyDatabase.class)
public class StoreProfileOffline extends BaseModel {
    @Column
    @PrimaryKey
    int id;
    @Column
    String fname;
    @Column
    String sname;
    @Column
    String bvnNumber;
    @Column
    String idType;
    @Column
    String idNumber;
    @Column
    String farmerId;
    @Column
    String bankName;
    @Column
    String govtString;
    @Column
    String nextOfKin;
    @Column
    boolean synced;
    @Column
    String nextOfKinPhone;
    @Column
    String role;
    @Column
    String dob;
    @Column
    String dobForPaga;
    @Column
    String gender;
    @Column
    String f_print;
    @Column
    String phone;
    @Column
    String marital_status;
    @Column
    String crop_proficiency;
    @Column
    String income_range;
    @Column
    String pro_image;
    @Column
    String pro_image_thumbnail;
    @Column
    String comment;
    @Column
    String acct_number;
    @Column
    String land_area_farmed;
    @Column
    String lat1;
    @Column
    String lat2;
    @Column
    String lat3;
    @Column
    String lat4;

    @Column
    boolean readWriteEnglish;
    @Column
    boolean haveSmartPhone;

    @Column
    String long1;
    @Column
    String long2;
    @Column
    String long3;
    @Column
    String long4;

    @Column
    int user_add_id;
    @Column
    int state_id;
    @Column
    int local_id;
    @Column
    String farm_group_id;
    @Column
    int farm_location_id;
    @Column
    int bank_id;
    @Column
    int number_of_dependants;
    @Column
    int years_of_experience;
    @Column
    String timeStamp;


    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getDobForPaga() {
        return dobForPaga;
    }

    public void setDobForPaga(String dobForPaga) {
        this.dobForPaga = dobForPaga;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getF_print() {
        return f_print;
    }

    public void setF_print(String f_print) {
        this.f_print = f_print;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public String getCrop_proficiency() {
        return crop_proficiency;
    }

    public void setCrop_proficiency(String crop_proficiency) {
        this.crop_proficiency = crop_proficiency;
    }

    public String getIncome_range() {
        return income_range;
    }

    public void setIncome_range(String income_range) {
        this.income_range = income_range;
    }

    public String getPro_image() {
        return pro_image;
    }

    public void setPro_image(String pro_image) {
        this.pro_image = pro_image;
    }

    public String getPro_image_thumbnail() {
        return pro_image_thumbnail;
    }

    public void setPro_image_thumbnail(String pro_image_thumbnail) {
        this.pro_image_thumbnail = pro_image_thumbnail;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAcct_number() {
        return acct_number;
    }

    public void setAcct_number(String acct_number) {
        this.acct_number = acct_number;
    }

    public String getLand_area_farmed() {
        return land_area_farmed;
    }

    public void setLand_area_farmed(String land_area_farmed) {
        this.land_area_farmed = land_area_farmed;
    }

    public int getUser_add_id() {
        return user_add_id;
    }

    public void setUser_add_id(int user_add_id) {
        this.user_add_id = user_add_id;
    }

    public int getState_id() {
        return state_id;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }

    public int getLocal_id() {
        return local_id;
    }

    public void setLocal_id(int local_id) {
        this.local_id = local_id;
    }

    public String getFarm_group_id() {
        return farm_group_id;
    }

    public void setFarm_group_id(String farm_group_id) {
        this.farm_group_id = farm_group_id;
    }

    public int getFarm_location_id() {
        return farm_location_id;
    }

    public void setFarm_location_id(int farm_location_id) {
        this.farm_location_id = farm_location_id;
    }

    public int getBank_id() {
        return bank_id;
    }

    public void setBank_id(int bank_id) {
        this.bank_id = bank_id;
    }

    public int getNumber_of_dependants() {
        return number_of_dependants;
    }

    public void setNumber_of_dependants(int number_of_dependants) {
        this.number_of_dependants = number_of_dependants;
    }

    public int getYears_of_experience() {
        return years_of_experience;
    }

    public void setYears_of_experience(int years_of_experience) {
        this.years_of_experience = years_of_experience;
    }


    public void setId(int id) {
        this.id = id;
    }


    public int getId() {
        return id;
    }

    public String getBvnNumber() {
        return bvnNumber;
    }

    public void setBvnNumber(String bvnNumber) {
        this.bvnNumber = bvnNumber;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(String farmerId) {
        this.farmerId = farmerId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getGovtString() {
        return govtString;
    }

    public void setGovtString(String govtString) {
        this.govtString = govtString;
    }

    public String getLat1() {
        return lat1;
    }

    public void setLat1(String lat1) {
        this.lat1 = lat1;
    }

    public String getLat2() {
        return lat2;
    }

    public void setLat2(String lat2) {
        this.lat2 = lat2;
    }

    public String getLat3() {
        return lat3;
    }

    public void setLat3(String lat3) {
        this.lat3 = lat3;
    }

    public String getLat4() {
        return lat4;
    }

    public void setLat4(String lat4) {
        this.lat4 = lat4;
    }

    public String getLong1() {
        return long1;
    }

    public void setLong1(String long1) {
        this.long1 = long1;
    }

    public String getLong2() {
        return long2;
    }

    public void setLong2(String long2) {
        this.long2 = long2;
    }

    public String getLong3() {
        return long3;
    }

    public void setLong3(String long3) {
        this.long3 = long3;
    }

    public String getLong4() {
        return long4;
    }

    public void setLong4(String long4) {
        this.long4 = long4;
    }

    public boolean isReadWriteEnglish() {
        return readWriteEnglish;
    }

    public void setReadWriteEnglish(boolean readWriteEnglish) {
        this.readWriteEnglish = readWriteEnglish;
    }

    public boolean isHaveSmartPhone() {
        return haveSmartPhone;
    }

    public void setHaveSmartPhone(boolean haveSmartPhone) {
        this.haveSmartPhone = haveSmartPhone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getNextOfKin() {
        return nextOfKin;
    }

    public void setNextOfKin(String nextOfKin) {
        this.nextOfKin = nextOfKin;
    }

    public String getNextOfKinPhone() {
        return nextOfKinPhone;
    }

    public void setNextOfKinPhone(String nextOfKinPhone) {
        this.nextOfKinPhone = nextOfKinPhone;
    }

    public boolean isSynced() {
        return synced;
    }

    public void setSynced(boolean synced) {
        this.synced = synced;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }
}
