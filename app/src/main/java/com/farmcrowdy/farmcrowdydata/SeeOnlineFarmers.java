package com.farmcrowdy.farmcrowdydata;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdydata.addFarmer.FullFarmerPojo;
import com.farmcrowdy.farmcrowdydata.dashboard.ListOfAllFamersBasedOnParams;
import com.farmcrowdy.farmcrowdydata.dashboard.SingleFarmerProfileActivity;
import com.farmcrowdy.farmcrowdydata.engine.TinyDB;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SeeOnlineFarmers extends AppCompatActivity {
    RecyclerView recyclerView;
    private static final int REQUEST_PHONE_CALL = 300;
    List<FullFarmerPojo> myStore;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_offline_farmers);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(view -> finish());
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading farmers online");
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(true);
        progressDialog.show();

        MyEndpoint myEndpoint = ApplicationInstance.getRetrofit().create(MyEndpoint.class);

        Call<ListFullfarmer> caller = myEndpoint.getFarmersByAgentFromAzure(new TinyDB(this).getInt("user_id", 1));
        caller.enqueue(new Callback<ListFullfarmer>() {
            @Override
            public void onResponse(Call<ListFullfarmer> call, Response<ListFullfarmer> response) {

                progressDialog.dismiss();
                progressDialog.cancel();

                if(response.isSuccessful()) {
                    myStore = response.body().getMessage();
                    TextView textView = findViewById(R.id.totally);
                    textView.setText(myStore.size()+" farmers");

                    recyclerView = findViewById(R.id.offlineFarmersRecyclerView);
                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SeeOnlineFarmers.this);
                    recyclerView.setLayoutManager(linearLayoutManager);
                    recyclerView.setHasFixedSize(true);
                    recyclerView.setAdapter(new OfflineRecyclerAdapter());
                }
                else {
                    Toast.makeText(SeeOnlineFarmers.this, "Not yet available for viewing, try again tomorrow", Toast.LENGTH_LONG).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<ListFullfarmer> call, Throwable t) {
                Toast.makeText(SeeOnlineFarmers.this, "Not yet available for viewing, try again tomorrow", Toast.LENGTH_LONG).show();
                finish();
            }
        });




       /* ApplicationInstance.getFirestore().collection("novusData")
                .whereEqualTo("user_add_id", new TinyDB(this).getInt("user_id", 1))
                .addSnapshotListener((queryDocumentSnapshots, e) -> {

                    progressDialog.dismiss();
                    progressDialog.cancel();

                    if(e==null) {
                        myStore = queryDocumentSnapshots.toObjects(FullFarmerPojo.class);
                        TextView textView = findViewById(R.id.totally);
                        textView.setText(myStore.size()+" farmers");

                        recyclerView = findViewById(R.id.offlineFarmersRecyclerView);
                        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SeeOnlineFarmers.this);
                        recyclerView.setLayoutManager(linearLayoutManager);
                        recyclerView.setHasFixedSize(true);
                        recyclerView.setAdapter(new OfflineRecyclerAdapter());

                    }
                    else {
                        Toast.makeText(SeeOnlineFarmers.this, "Something went wrong, please try again later with stronger internet connectivity", Toast.LENGTH_SHORT).show();
                    }

                });
*/

    }
    private class OfflineRecyclerAdapter extends RecyclerView.Adapter<OfflineRecyclerAdapter.OfflineViewHolder> {
        @NonNull
        @Override
        public OfflineRecyclerAdapter.OfflineViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = getLayoutInflater().inflate(R.layout.offline_farmers_items, viewGroup, false);
            return new OfflineRecyclerAdapter.OfflineViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final OfflineRecyclerAdapter.OfflineViewHolder offlineViewHolder, int i) {
            offlineViewHolder.nameText.setText(myStore.get(i).getFname());
            offlineViewHolder.text1.setText(myStore.get(i).getFarmerId());
            offlineViewHolder.text2.setText(myStore.get(i).getCrop_proficiency()+" Farmer");
            offlineViewHolder.textGovt.setText("Location: "+myStore.get(i).getGovtString());
            offlineViewHolder.textSyned.setText("Data backed up online");
            offlineViewHolder.caller.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    caller(myStore.get(offlineViewHolder.getAdapterPosition()).getPhone());
                }
            });
            Picasso.with(SeeOnlineFarmers.this).load(myStore.get(i).getPro_image()).placeholder(R.drawable.ic_accout_blue_24dp).into(offlineViewHolder.imageView);
            //offlineViewHolder.imageView.setImageURI(Uri.parse(""));

            offlineViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(SeeOnlineFarmers.this, SingleFarmerProfileActivity.class);
                    intent.putExtra("data", myStore.get(offlineViewHolder.getAdapterPosition()));
                    startActivity(intent);
                }
            });

        }

        @Override
        public int getItemCount() {
            return myStore.size();
        }

        public class OfflineViewHolder extends RecyclerView.ViewHolder {
            CircleImageView imageView;
            TextView nameText, text1, text2, textGovt, textSyned;
            ImageView caller;
            public OfflineViewHolder(@NonNull View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.offline_dp);
                nameText = itemView.findViewById(R.id.offline_name);
                text1 = itemView.findViewById(R.id.another1);
                text2 = itemView.findViewById(R.id.another2);
                textGovt = itemView.findViewById(R.id.another3);
                textSyned = itemView.findViewById(R.id.synced);
                caller = itemView.findViewById(R.id.callertune);
            }
        }
    }
    void caller(String number) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
                return;

            }
            startActivity(callIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e("Calling a Phone Number", "Call failed", activityException);
        }
    }
    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PHONE_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(SeeOnlineFarmers.this, "Tap the call button again", Toast.LENGTH_SHORT).show();
                } else {

                }
                return;
            }
        }
    }
}
