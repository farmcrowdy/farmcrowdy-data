package com.farmcrowdy.farmcrowdydata;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdydata.addFarmer.FullFarmerPojo;
import com.farmcrowdy.farmcrowdydata.addFarmer.ProfileActivity;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline_Table;
import com.farmcrowdy.farmcrowdydata.dashboard.BackUpInterface;
import com.farmcrowdy.farmcrowdydata.dashboard.BackUpPresenterImpl;
import com.farmcrowdy.farmcrowdydata.dashboard.DashboardActivity;
import com.farmcrowdy.farmcrowdydata.engine.LoadEffecient;
import com.farmcrowdy.farmcrowdydata.engine.TinyDB;
import com.farmcrowdy.farmcrowdydata.login.LoginActivity;
import com.farmcrowdy.farmcrowdydata.seeFarmGroups.CreateFarmGroup;
import com.farmcrowdy.farmcrowdydata.seeFarmGroups.SeeFarmGroupsActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

public class MainActivity extends AppCompatActivity implements BackUpInterface{

    TinyDB tinyDB;
    ProgressDialog progressDialog;
    int counter = 0;

    boolean builderHasShown = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tinyDB = new TinyDB(this);
        progressDialog = new ProgressDialog(this);
        if(tinyDB.getString("user_email") == null || tinyDB.getString("user_email").isEmpty()) {
            startActivity(new Intent(this, LoginActivity.class));
            finishAffinity();
        }



       // startActivity(new Intent(this, EnrollActivity.class));
        checkForUpdates();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);

        if(tinyDB.getString("user_email").equals("gift.adeyemi@novusagro.com") ||
                tinyDB.getString("user_email").equals("joyce.fred@novusagro.com") ||
        tinyDB.getString("user_email").equals("uwem.asuquo@novusagro.com") ||
        tinyDB.getString("user_email").equals("tosin.odulawa@farmcrowdy.com") ||
        tinyDB.getString("user_email").equals("uwemedimomichael.asuquo@gmail.com") ||
        tinyDB.getString("user_email").equals("sa_adeoye@yahoo.co.uk") ||
                tinyDB.getString("user_email").equals("anndarey@gmail.com") ||
        tinyDB.getString("user_email").equals("christopher.abiodun@farmcrowdy.com") ||
        tinyDB.getString("user_email").equals("ifeanyi.anazodo@farmcrowdy.com")) {
                    findViewById(R.id.sevenTab).setVisibility(View.VISIBLE);
        }

        doSomeClickings();
    }
    void doSomeClickings() {
        findViewById(R.id.addFarmer_tabber).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, ProfileActivity.class));

            }
        });
        findViewById(R.id.farmGroups_id_tab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ViewOfflineFarmers.class);
                intent.putExtra("groupName", "empty");
                startActivity(intent);
            }
        });
        findViewById(R.id.farmUpdates_tabs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SeeFarmGroupsActivity.class));
            }
        });
        findViewById(R.id.fifthTab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, CreateFarmGroup.class));
            }
        });

        findViewById(R.id.sevenTab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, DashboardActivity.class));
            }
        });
        findViewById(R.id.sixTab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new TinyDB(MainActivity.this).clear();
                Toast.makeText(MainActivity.this, "Logging out", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finishAffinity();
            }
        });
        findViewById(R.id.eightTab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, SeeOnlineFarmers.class));
            }
        });

        findViewById(R.id.Operations_id_tab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.setMessage("Preparing to upload farmer's data online...");
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.setCancelable(false);
                progressDialog.show();
                //ApplicationInstance.getFirebaseAuth().signInAnonymously();

                final List<StoreProfileOffline> myStore = SQLite.select().from(StoreProfileOffline.class).where(StoreProfileOffline_Table.synced.is(false)).queryList();

                if (myStore.isEmpty()) {
                    progressDialog.dismiss();
                    progressDialog.cancel();

                    Toast.makeText(MainActivity.this, "All farmers have been synced", Toast.LENGTH_SHORT).show();
                }

                else{
                    BackUpPresenterImpl backUpPresenter = new BackUpPresenterImpl(MainActivity.this);
                    backUpPresenter.doSynced();
                }


                //progressDialog.show();
               // mainPresenter.doSyncOnline();
            }
        });

    }


    void checkForUpdates() {
        ApplicationInstance.getFirestore().collection("versionCheck").document("withNovus").addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
           if(e==null) {

               try{
                   int latestVersion = Integer.valueOf(documentSnapshot.getData().get("code").toString());
                   if(latestVersion > 23 ) {
                       AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                       builder.setTitle("Update your app now");
                       builder.setCancelable(false);
                       builder.setMessage("This version of data collecting app has expired. You need to download, and install to update a new version. Contact admin for more information or click the button below");
                       builder.setPositiveButton("Update app", (dialogInterface, i) -> {

                           dialogInterface.dismiss();
                           dialogInterface.cancel();

                           String url = "https://bit.ly/updateFcdyData2";
                           Intent tent = new Intent(Intent.ACTION_VIEW);
                           tent.setData(Uri.parse(url));
                           startActivity(tent);
                           finishAffinity();

                       });
                       builder.show();
                   }
               }
               catch (Exception er) {

               }

           }

            }
        });



    }


    @Override
    public void bankUpNow(int status) {
        switch (status) {
            case 1: if (progressDialog !=null) {

                progressDialog.dismiss();
                progressDialog.cancel();
            }

                AlertDialog.Builder builderSuccess  = new AlertDialog.Builder(MainActivity.this);
                builderSuccess.setMessage("Uploading to the server completed successfully");
                builderSuccess.setTitle("Successful");
                builderSuccess.setPositiveButton("Done", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        dialogInterface.cancel();
                    }
                });
                builderSuccess.show();

                break;
            case 2:
                if (progressDialog !=null) {
                    progressDialog.dismiss();
                    progressDialog.cancel();
                }
                    AlertDialog.Builder builder2 = new AlertDialog.Builder(MainActivity.this);
                    builder2.setMessage("Something went wrong while uploading...");
                    builder2.setTitle("Failed");
                    builder2.setPositiveButton("Try again", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                            dialogInterface.cancel();
                        }
                    });
                    builder2.show();

                break;
        }
    }
}
