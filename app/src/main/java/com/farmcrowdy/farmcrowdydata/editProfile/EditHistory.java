package com.farmcrowdy.farmcrowdydata.editProfile;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.addFarmer.FarmingHistoryActivity;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline_Table;
import com.rafakob.floatingedittext.FloatingEditText;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.vishalsojitra.easylocation.EasyLocationAppCompatActivity;

public class EditHistory extends AppCompatActivity {
    FloatingEditText landArea, yearsOfExperience;
    Spinner incomeRange;
    StoreProfileOffline storeProfileOffline;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_3);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        String farmerId= getIntent().getExtras().getString("data");
        storeProfileOffline = SQLite.select().from(StoreProfileOffline.class).where(StoreProfileOffline_Table.farmerId.is(farmerId)).querySingle();

        incomeRange = findViewById(R.id.income_range_spinner);
        landArea = findViewById(R.id.farmed_previously);
        yearsOfExperience = findViewById(R.id.years_of_experience_edit);
        landArea.setText(storeProfileOffline.getLand_area_farmed());
        yearsOfExperience.setText(String.valueOf(storeProfileOffline.getYears_of_experience()));

        int resultOfIncome = 0;

        switch (storeProfileOffline.getIncome_range()) {
            case "0 - 99,000": resultOfIncome  = 0;
                break;
            case "100,000 - 500,000": resultOfIncome  = 1;
                break;
            case "500,000 - 1,000,000": resultOfIncome  = 2;
                break;
            case "> 1,000,000": resultOfIncome  = 3;
                break;
        }
        incomeRange.setSelection(resultOfIncome);

        findViewById(R.id.fab).setOnClickListener(view -> {
            incomeRange.getSelectedItem().toString();
            if(landArea.getText().length()>0) {
                if(!yearsOfExperience.getText().isEmpty()) {
                    storeProfileOffline.setIncome_range(incomeRange.getSelectedItem().toString());
                    storeProfileOffline.setLand_area_farmed(landArea.getText());
                    storeProfileOffline.setYears_of_experience(Integer.valueOf(yearsOfExperience.getText()));

                    storeProfileOffline.update();

                    Toast.makeText(EditHistory.this, "Update successful", Toast.LENGTH_SHORT).show();
                    finish();
                }
                else {
                    Toast.makeText(EditHistory.this, "Please include years of experience of farmer", Toast.LENGTH_SHORT).show();
                }
            }
            else Toast.makeText(EditHistory.this, "Land area cannot be less than 1 hectare", Toast.LENGTH_SHORT).show();
        });


    }

}
