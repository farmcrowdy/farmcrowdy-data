package com.farmcrowdy.farmcrowdydata.editProfile;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdydata.ApplicationInstance;
import com.farmcrowdy.farmcrowdydata.DirectPojo.CropPojo;
import com.farmcrowdy.farmcrowdydata.DirectPojo.LocalGovtPojo;
import com.farmcrowdy.farmcrowdydata.DirectPojo.LocalGovtPojo_Table;
import com.farmcrowdy.farmcrowdydata.DirectPojo.StatePojo;
import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.addFarmer.BankAndIdentificationActivity;
import com.farmcrowdy.farmcrowdydata.addFarmer.FullFarmerPojo;
import com.farmcrowdy.farmcrowdydata.addFarmer.ProfileActivity;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreFarmGroups;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline_Table;
import com.farmcrowdy.farmcrowdydata.engine.LoadEffecient;
import com.farmcrowdy.farmcrowdydata.engine.TinyDB;
import com.rafakob.floatingedittext.FloatingEditText;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends AppCompatActivity {
    FloatingEditText firstnameEdit, phoneNumberEdit, noOfDependantsEdit, nextOfKin, nextOfKinPhone;
    Spinner genderspinner, maritalStatus, cropSpinner, locationSpinner,  stateSpinner,
            localGovtSpinner, yearSpinner, monthSpinner, daySpinner, groupSpinner;


    String genderString, maritalString, cropString, govtString, stateString, groupString;
    int cropInt, stateInt, govtInt;

    ArrayList<String> cropListString = new ArrayList<>();
    ArrayList<String> farmgroupERList = new ArrayList<>();
    ArrayList<Integer> cropListInt = new ArrayList<>();

    ArrayList<String> stateListString = new ArrayList<>();
    ArrayList<Integer> stateListInt = new ArrayList<>();

    ArrayList<String> localGovtListString = new ArrayList<>();
    ArrayList<Integer> localGovtListInt = new ArrayList<>();
    private String mYear, mMonth, mDay;
    String imageList="";
    
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_1);

        String farmerId = getIntent().getExtras().getString("data");
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        StoreProfileOffline storeProfileOffline1 = SQLite.select().from(StoreProfileOffline.class).where(StoreProfileOffline_Table.farmerId.is(farmerId)).querySingle();


        firstnameEdit = findViewById(R.id.fullname_editText);
        phoneNumberEdit = findViewById(R.id.phone_editText);
        noOfDependantsEdit = findViewById(R.id.dependant_editText);
        genderspinner = findViewById(R.id.genderSpinner);
        cropSpinner = findViewById(R.id.proficiency_spinner);
        locationSpinner = findViewById(R.id.farm_location_spinner);
        stateSpinner = findViewById(R.id.state_spinner);
        maritalStatus = findViewById(R.id.spinnerMaritalStatus);
        localGovtSpinner = findViewById(R.id.LGA_spinner);
        yearSpinner = findViewById(R.id.yearSpinner);
        monthSpinner = findViewById(R.id.monthSpinner);
        daySpinner = findViewById(R.id.daySpinner);
        groupSpinner = findViewById(R.id.group_spinner);
        nextOfKin = findViewById(R.id.nextOfKin_editText);
        nextOfKinPhone = findViewById(R.id.nextOfKinPhone_editText);

        yearSpinner.setVisibility(View.GONE);
        monthSpinner.setVisibility(View.GONE);
        daySpinner.setVisibility(View.GONE);
        CircleImageView circleImageView = findViewById(R.id.selected_photo);
        circleImageView.setImageURI(Uri.parse(storeProfileOffline1.getPro_image()));

        updateInformation(storeProfileOffline1);
        getAddings(storeProfileOffline1);
        setupAllSpinners(storeProfileOffline1);
        findViewById(R.id.fab).setOnClickListener(view -> {
            if(firstnameEdit.getText().length() > 5) {
                if(phoneNumberEdit.getText().trim().length() == 11) {
                    if(!mYear.equals("Year") || !mMonth.equals("Month") ||!mDay.equals("Day")) {
                        if(cropString !=null && govtString !=null && stateString !=null) {
                            if(groupString !=null) {
                                if(!noOfDependantsEdit.getText().isEmpty()) {
                                    if(!imageList.isEmpty()) {
                                        if(!nextOfKin.getText().isEmpty()) {
                                            if(!nextOfKinPhone.getText().isEmpty()) {

                                                storeProfileOffline1.setFname(firstnameEdit.getText());
                                                storeProfileOffline1.setGovtString(govtString);
                                                storeProfileOffline1.setPhone(phoneNumberEdit.getText());
                                                storeProfileOffline1.setNumber_of_dependants(Integer.valueOf(noOfDependantsEdit.getText()));
                                                storeProfileOffline1.setDob(mDay+" "+mMonth+", "+mYear);
                                                storeProfileOffline1.setGender(genderString);
                                                storeProfileOffline1.setCrop_proficiency(cropString);
                                                storeProfileOffline1.setFarm_group_id(groupString.toLowerCase());
                                                storeProfileOffline1.setState_id(stateInt);
                                                storeProfileOffline1.setMarital_status(maritalString);
                                                storeProfileOffline1.setLocal_id(govtInt);
                                                storeProfileOffline1.setUser_add_id(new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1));
                                                storeProfileOffline1.setPro_image(imageList);
                                                storeProfileOffline1.setNextOfKin(nextOfKin.getText());
                                                storeProfileOffline1.setNextOfKinPhone(nextOfKinPhone.getText());

                                                storeProfileOffline1.update();
                                                Toast.makeText(EditProfileActivity.this, "Update successful", Toast.LENGTH_SHORT).show();
                                                finish();

                                            }else Toast.makeText(EditProfileActivity.this, "Please, next of kin Phone number is required", Toast.LENGTH_SHORT).show();
                                        } else Toast.makeText(EditProfileActivity.this, "Please, next of kin name is required", Toast.LENGTH_SHORT).show();
                                    }  else  Toast.makeText(EditProfileActivity.this, "Please take or upload farmer's picture", Toast.LENGTH_SHORT).show();
                                } else Toast.makeText(EditProfileActivity.this, "Please provide Number of Dependant", Toast.LENGTH_SHORT).show();
                            } else Toast.makeText(EditProfileActivity.this, "Farm group name too short", Toast.LENGTH_SHORT).show();
                        } else Toast.makeText(EditProfileActivity.this, "Please select the crop type, state and local government", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(EditProfileActivity.this, "Please select date of birth", Toast.LENGTH_SHORT).show();
                    }
                }
                else Toast.makeText(EditProfileActivity.this, "Phone number incorrect", Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(EditProfileActivity.this, "Full name too short", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void updateInformation(StoreProfileOffline storeProfileOffline) {
        firstnameEdit.setText(storeProfileOffline.getFname());
        phoneNumberEdit.setText(storeProfileOffline.getPhone());
        nextOfKin.setText(storeProfileOffline.getNextOfKin());
        nextOfKinPhone.setText(storeProfileOffline.getNextOfKinPhone());
        noOfDependantsEdit.setText(String.valueOf(storeProfileOffline.getNumber_of_dependants()));

    }

    private void getAddings(StoreProfileOffline storeProfileOffline) {

        List<StoreFarmGroups> farmGroupList = SQLite.select().
                from(StoreFarmGroups.class).queryList();

            farmgroupERList.add(storeProfileOffline.getFarm_group_id());
        for(StoreFarmGroups group : farmGroupList){
            farmgroupERList.add(group.getGroupName());
        }


        List<CropPojo> groupList = SQLite.select().
                from(CropPojo.class).queryList();

        cropListString = new ArrayList<>();
        cropListInt = new ArrayList<>();

        cropListString.add(storeProfileOffline.getCrop_proficiency());
        cropListInt.add(0);
        for(int i=0; i<groupList.size(); i++){
            cropListString.add(groupList.get(i).getCrop_name());
            cropListInt.add(groupList.get(i).getCrop_id());
            Log.d("DO_TESTING", "this is an updated for number of crops"+cropListString.get(i));
        }



        List<StatePojo> stateList = SQLite.select().from(StatePojo.class).queryList();
        stateListInt.add(storeProfileOffline.getState_id());
        stateListString.add("Edit state");

        for(int i=0; i<stateList.size(); i++){
            stateListString.add(stateList.get(i).getName());
            stateListInt.add(stateList.get(i).getState_id());
            Log.d("DO_TESTING", "this is an updated for number of states"+stateListString.get(i));
        }

        findViewById(R.id.takeUploadPhoto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(EditProfileActivity.this);
            }
        });


    }
    private void setupAllSpinners(StoreProfileOffline storeProfileOffline) {
        genderspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                genderString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        groupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                groupString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mYear = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mMonth = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        daySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mDay = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        maritalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                maritalString = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ArrayAdapter<String> dataAdapterGroup = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, farmgroupERList);
        dataAdapterGroup.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        groupSpinner.setAdapter(dataAdapterGroup);


        ArrayAdapter<String> dataAdapterState = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, stateListString);
        dataAdapterState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        stateSpinner.setAdapter(dataAdapterState);



        stateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stateInt = stateListInt.get(i);
                stateString = adapterView.getItemAtPosition(i).toString();
                if(i!=0) {

                    List<LocalGovtPojo> govtList = SQLite.select().from(LocalGovtPojo.class).where(LocalGovtPojo_Table.state_id.is(stateInt)).
                            queryList();
                    localGovtListString = new ArrayList<>();
                    localGovtListString.add(storeProfileOffline.getGovtString());
                    localGovtListInt.add(storeProfileOffline.getLocal_id());
                    for(int p=0; p<govtList.size(); p++) {
                        localGovtListString.add(govtList.get(p).getLocal_name());
                        localGovtListInt.add(govtList.get(p).getLocal_id());
                    }

                    ArrayAdapter<String> dataAdapterGovt = new ArrayAdapter<String>(EditProfileActivity.this,
                            android.R.layout.simple_spinner_item, localGovtListString);
                    dataAdapterGovt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    localGovtSpinner.setAdapter(dataAdapterGovt);

                    localGovtSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            govtInt = localGovtListInt.get(i);
                            govtString = adapterView.getItemAtPosition(i).toString();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });



        cropListString.add(storeProfileOffline.getCrop_proficiency());
        cropListInt.add(0);
        cropSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i!=0) {
                    //cropInt = cropListInt.get(i);
                    cropString = adapterView.getItemAtPosition(i).toString();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imageList = resultUri.getPath();
                if (!imageList.equals("empty")) {
                    Bitmap high;
                    try {
                        high = new LoadEffecient().decodeSampledBitmapFromFile(imageList, 200, 200);
                        File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
                        CircleImageView circleImageView = findViewById(R.id.selected_photo);
                        circleImageView.setImageURI(Uri.parse(file.getPath()));

                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
