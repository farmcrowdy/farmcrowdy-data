package com.farmcrowdy.farmcrowdydata.editProfile;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdydata.DirectPojo.BanksJojo;
import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.addFarmer.BankAndIdentificationActivity;
import com.farmcrowdy.farmcrowdydata.addFarmer.FarmingHistoryActivity;
import com.farmcrowdy.farmcrowdydata.addFarmer.FullFarmerPojo;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline_Table;
import com.rafakob.floatingedittext.FloatingEditText;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;
import java.util.List;

public class EditBankDetails extends AppCompatActivity {
    int idExpectedLength = 1;
    Spinner listOfBanks, typeOfIdentification, roleSpinner;
    ArrayList<String> bankListString;
    ArrayList<Integer> bankListInt;
    int bank_selected;
    String methodIdString, bankName;
    Switch readWrite, smartPhone;
    StoreProfileOffline storeProfileOffline;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_2);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        String farmerId= getIntent().getExtras().getString("data");
        storeProfileOffline = SQLite.select().from(StoreProfileOffline.class).where(StoreProfileOffline_Table.farmerId.is(farmerId)).querySingle();

        typeOfIdentification = findViewById(R.id.methodid_spinner);

        final FloatingEditText accountNumberEdit = findViewById(R.id.account_number_editText);
        final FloatingEditText bvnEdit = findViewById(R.id.bvn_editText);
        readWrite = findViewById(R.id.readWrite);
        smartPhone = findViewById(R.id.smartPhone);
        roleSpinner = findViewById(R.id.roleSpinner);
        final FloatingEditText methodIdEdit = findViewById(R.id.method_id_editText);

        methodIdEdit.setText(storeProfileOffline.getIdNumber());
        bvnEdit.setText(storeProfileOffline.getBvnNumber());
        accountNumberEdit.setText(storeProfileOffline.getAcct_number());
        readWrite.setChecked(storeProfileOffline.isReadWriteEnglish());
        smartPhone.setChecked(storeProfileOffline.isHaveSmartPhone());


        getBankAdding();
        findViewById(R.id.fab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(accountNumberEdit.getText().length() > 8) {
                    if(bvnEdit.getText().length() == 11) {
                        if(methodIdEdit.getText().length() == idExpectedLength) {
                            storeProfileOffline.setBank_id(bank_selected);
                            storeProfileOffline.setAcct_number(accountNumberEdit.getText());
                            storeProfileOffline.setBankName(bankName);
                            storeProfileOffline.setBvnNumber(bvnEdit.getText());
                            storeProfileOffline.setIdType(methodIdString);
                            storeProfileOffline.setIdNumber(methodIdEdit.getText());
                            storeProfileOffline.setHaveSmartPhone(smartPhone.isChecked());
                            storeProfileOffline.setReadWriteEnglish(readWrite.isChecked());
                            storeProfileOffline.setRole(roleSpinner.getSelectedItem().toString());


                            storeProfileOffline.update();
                            Toast.makeText(EditBankDetails.this, "Update successful", Toast.LENGTH_SHORT).show();
                            finish();

                        }
                        else {
                            Toast.makeText(EditBankDetails.this, "Method of identification number not complete: "+idExpectedLength+" chars are expected", Toast.LENGTH_SHORT).show();
                        }
                    }
                    else Toast.makeText(EditBankDetails.this, "BVN number not complete 11 chars are expected", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(EditBankDetails.this, "Account number incorrect", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    void getBankAdding() {
        listOfBanks = findViewById(R.id.banks_spinner);
        List<BanksJojo> groupList = SQLite.select().
                from(BanksJojo.class).queryList();

        bankListString = new ArrayList<>();
        bankListInt = new ArrayList<>();
        bankListString.add(storeProfileOffline.getBankName());
        bankListInt.add(storeProfileOffline.getBank_id());

        for(int i=0; i<groupList.size(); i++){
            bankListString.add(groupList.get(i).getBank_name());
            bankListInt.add(groupList.get(i).getBank_id());
        }
        ArrayAdapter<String> dataAdapterState = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, bankListString);
        dataAdapterState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        listOfBanks.setAdapter(dataAdapterState);
        listOfBanks.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                bank_selected = bankListInt.get(i);
                bankName = adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
            int resultOfRole = 0;
        switch (storeProfileOffline.getRole()) {
            case "Chairman": resultOfRole  = 0;
                break;
            case "Secretary": resultOfRole  = 1;
                break;
            case "Member": resultOfRole  = 2;
                break;
        }

        int idOfIdent = 0;
        switch (storeProfileOffline.getIdType()) {
            case  "Voters card" : idOfIdent = 0;
            break;
            case "National ID card": idOfIdent = 1;
            break;
            case "International Passport": idOfIdent = 2;
            break;
        }

        roleSpinner.setSelection(resultOfRole);
        typeOfIdentification.setSelection(idOfIdent);

        typeOfIdentification.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                methodIdString = adapterView.getItemAtPosition(i).toString();
                switch (i) {
                    case 0: idExpectedLength = 19;
                        break;
                    case 1: idExpectedLength = 12;
                        break;
                    case 2: idExpectedLength = 9;
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
