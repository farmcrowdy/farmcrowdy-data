package com.farmcrowdy.farmcrowdydata.seeFarmGroups;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.ViewOfflineFarmers;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreFarmGroups;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

public class SeeFarmGroupsActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.see_farmgroups);

        List<StoreFarmGroups> myStore = SQLite.select().from(StoreFarmGroups.class).queryList();

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        recyclerView = findViewById(R.id.farmgroupRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new SeeFarmGroupRecyclerAdapter(myStore));
    }

    private class SeeFarmGroupRecyclerAdapter extends RecyclerView.Adapter<SeeFarmGroupRecyclerAdapter.SeeFarmGroupViewHolder> {

        List<StoreFarmGroups> myStore;
        SeeFarmGroupRecyclerAdapter(List<StoreFarmGroups> myStore ) {
            this.myStore = myStore;
        }


        @NonNull
        @Override
        public SeeFarmGroupViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = getLayoutInflater().inflate(R.layout.farmgroup_items, viewGroup, false);
            return new SeeFarmGroupViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final SeeFarmGroupViewHolder seeFarmGroupViewHolder, int i) {
            seeFarmGroupViewHolder.groupName.setText(myStore.get(i).getGroupName());
            seeFarmGroupViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(SeeFarmGroupsActivity.this, ViewOfflineFarmers.class);
                    intent.putExtra("groupName", myStore.get(seeFarmGroupViewHolder.getAdapterPosition()).getGroupName());
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return myStore.size();
        }

        class SeeFarmGroupViewHolder extends RecyclerView.ViewHolder {
            TextView groupName;
            public SeeFarmGroupViewHolder(@NonNull View itemView) {
                super(itemView);
                groupName = itemView.findViewById(R.id.group_name);
            }
        }

    }
}
