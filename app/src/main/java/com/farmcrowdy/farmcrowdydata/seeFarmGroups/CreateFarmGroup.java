package com.farmcrowdy.farmcrowdydata.seeFarmGroups;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreFarmGroups;
import com.farmcrowdy.farmcrowdydata.engine.TinyDB;
import com.rafakob.floatingedittext.FloatingEditText;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.List;

public class CreateFarmGroup extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_farm_group);
        final FloatingEditText groupName = findViewById(R.id.newGroupName);
        Button createButton = findViewById(R.id.createGroup);

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(groupName.getText().length() > 4) {
                    List<StoreFarmGroups> myStore = SQLite.select().from(StoreFarmGroups.class).queryList();
                    int userId = new TinyDB(CreateFarmGroup.this).getInt("user_id", 1);
                    String adjusted =   userId+ "_"+groupName.getText().toLowerCase();
                    StoreFarmGroups storeFarmGroups = new StoreFarmGroups();
                    storeFarmGroups.setGroupName(adjusted);
                    storeFarmGroups.setUser_id(userId);
                    storeFarmGroups.setId(myStore.size() + 1);
                    storeFarmGroups.save();

                    Toast.makeText(CreateFarmGroup.this, "Group created successfully", Toast.LENGTH_SHORT).show();
                    finish();
                }
                else {
                    Toast.makeText(CreateFarmGroup.this, "Group name is a minimum of 4 chars", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
