package com.farmcrowdy.farmcrowdydata;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.farmcrowdy.farmcrowdydata.addFarmer.FullFarmerPojo;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreFarmGroups;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline;
import com.farmcrowdy.farmcrowdydata.addFarmer.StoreProfileOffline_Table;
import com.farmcrowdy.farmcrowdydata.dashboard.SingleFarmerProfileActivity;
import com.farmcrowdy.farmcrowdydata.editProfile.EditBankDetails;
import com.farmcrowdy.farmcrowdydata.editProfile.EditHistory;
import com.farmcrowdy.farmcrowdydata.editProfile.EditProfileActivity;
import com.farmcrowdy.farmcrowdydata.engine.LoadEffecient;
import com.farmcrowdy.farmcrowdydata.engine.TinyDB;
import com.farmcrowdy.farmcrowdydata.seeFarmGroups.SeeFarmGroupsActivity;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ViewOfflineFarmers extends AppCompatActivity {
    RecyclerView recyclerView;
    private static final int REQUEST_PHONE_CALL = 300;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_offline_farmers);

        int tfs_id = new TinyDB(ApplicationInstance.getContext()).getInt("user_id", 1);
        String groupName = getIntent().getExtras().getString("groupName");
        List<StoreProfileOffline> myStore = null;
        if(groupName.equals("empty")) myStore = SQLite.select().from(StoreProfileOffline.class).orderBy(StoreProfileOffline_Table.id, false).queryList();
        else myStore = SQLite.select().from(StoreProfileOffline.class).where(StoreProfileOffline_Table.farm_group_id.is(groupName)).orderBy(StoreProfileOffline_Table.id, false).queryList();

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        recyclerView = findViewById(R.id.offlineFarmersRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new OfflineRecyclerAdapter(myStore));

    }

    private class OfflineRecyclerAdapter extends RecyclerView.Adapter<OfflineRecyclerAdapter.OfflineViewHolder> {
        List<StoreProfileOffline> myStore;
        OfflineRecyclerAdapter(List<StoreProfileOffline> myStore ) {
            this.myStore = myStore;
        }
        @NonNull
        @Override
        public OfflineViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = getLayoutInflater().inflate(R.layout.offline_farmers_items, viewGroup, false);
            return new OfflineViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final OfflineViewHolder offlineViewHolder, int i) {
            offlineViewHolder.nameText.setText(myStore.get(i).getFname());
            offlineViewHolder.text1.setText("Farmer ID: "+myStore.get(i).getFarmerId());
            offlineViewHolder.text2.setText("Farm type: "+myStore.get(i).getCrop_proficiency());
            offlineViewHolder.textGovt.setText("Location: "+myStore.get(i).getGovtString());
            offlineViewHolder.textSyned.setText(myStore.get(i).isSynced() ? "Synced" : "Still offline");
            offlineViewHolder.caller.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    caller(myStore.get(offlineViewHolder.getAdapterPosition()).getPhone());
                }
            });

            Bitmap high = null;

            /*try {
                if(new File(myStore.get(i).getPro_image()).exists()) {
                    high = new LoadEffecient().decodeSampledBitmapFromFile(myStore.get(i).getPro_image(), LoadEffecient.maxHeight, LoadEffecient.maxWidth);
                    File file = new LoadEffecient().saveLocallyFromBitmapToTempFile(high, "bitmapHigh");
                    offlineViewHolder.imageView.setImageURI(Uri.parse(file.getPath()));
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            */

            if(!offlineViewHolder.textSyned.getText().equals("Synced")) {
                offlineViewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        popUpDialog(myStore.get(offlineViewHolder.getAdapterPosition()).getFarmerId());
                        return true;
                    }
                });
            }
            offlineViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ViewOfflineFarmers.this, SingleFarmerProfileActivity.class);
                    FullFarmerPojo fullFarmerPojo = new FullFarmerPojo();
                    fullFarmerPojo.setRole(myStore.get(offlineViewHolder.getAdapterPosition()).getRole());
                    fullFarmerPojo.setReadWriteEnglish(myStore.get(offlineViewHolder.getAdapterPosition()).isReadWriteEnglish());
                    fullFarmerPojo.setHaveSmartPhone(myStore.get(offlineViewHolder.getAdapterPosition()).isHaveSmartPhone());
                    fullFarmerPojo.setUser_add_id(myStore.get(offlineViewHolder.getAdapterPosition()).getUser_add_id());
                    fullFarmerPojo.setGovtString(myStore.get(offlineViewHolder.getAdapterPosition()).getGovtString());
                    fullFarmerPojo.setYears_of_experience(myStore.get(offlineViewHolder.getAdapterPosition()).getYears_of_experience());
                    fullFarmerPojo.setIncome_range(myStore.get(offlineViewHolder.getAdapterPosition()).getIncome_range());
                    fullFarmerPojo.setIdNumber(myStore.get(offlineViewHolder.getAdapterPosition()).getIdNumber());
                    fullFarmerPojo.setIdType(myStore.get(offlineViewHolder.getAdapterPosition()).getIdType());
                    fullFarmerPojo.setLand_area_farmed(myStore.get(offlineViewHolder.getAdapterPosition()).getLand_area_farmed());
                    fullFarmerPojo.setBvnNumber(myStore.get(offlineViewHolder.getAdapterPosition()).getBvnNumber());
                    fullFarmerPojo.setAcct_number(myStore.get(offlineViewHolder.getAdapterPosition()).getAcct_number());
                    fullFarmerPojo.setBankName(myStore.get(offlineViewHolder.getAdapterPosition()).getBankName());
                    fullFarmerPojo.setBank_id(myStore.get(offlineViewHolder.getAdapterPosition()).getBank_id());
                    fullFarmerPojo.setFname(myStore.get(offlineViewHolder.getAdapterPosition()).getFname());
                    fullFarmerPojo.setPhone(myStore.get(offlineViewHolder.getAdapterPosition()).getPhone());
                    fullFarmerPojo.setState_id(myStore.get(offlineViewHolder.getAdapterPosition()).getState_id());
                    fullFarmerPojo.setDob(myStore.get(offlineViewHolder.getAdapterPosition()).getDob());
                    fullFarmerPojo.setPro_image(myStore.get(offlineViewHolder.getAdapterPosition()).getPro_image());
                    fullFarmerPojo.setMarital_status(myStore.get(offlineViewHolder.getAdapterPosition()).getMarital_status());
                    fullFarmerPojo.setLocal_id(myStore.get(offlineViewHolder.getAdapterPosition()).getLocal_id());
                    fullFarmerPojo.setGender(myStore.get(offlineViewHolder.getAdapterPosition()).getGender());
                    fullFarmerPojo.setNumber_of_dependants(myStore.get(offlineViewHolder.getAdapterPosition()).getNumber_of_dependants());
                    fullFarmerPojo.setFarm_group_id(myStore.get(offlineViewHolder.getAdapterPosition()).getFarm_group_id());
                    fullFarmerPojo.setCrop_proficiency(myStore.get(offlineViewHolder.getAdapterPosition()).getCrop_proficiency());
                    fullFarmerPojo.setFarmerId(myStore.get(offlineViewHolder.getAdapterPosition()).getFarmerId());
                    fullFarmerPojo.setLat1(myStore.get(offlineViewHolder.getAdapterPosition()).getLat1());
                    fullFarmerPojo.setLat2(myStore.get(offlineViewHolder.getAdapterPosition()).getLat2());
                    fullFarmerPojo.setLat3(myStore.get(offlineViewHolder.getAdapterPosition()).getLat3());
                    fullFarmerPojo.setLat4(myStore.get(offlineViewHolder.getAdapterPosition()).getLat4());
                    fullFarmerPojo.setLong1(myStore.get(offlineViewHolder.getAdapterPosition()).getLong1());
                    fullFarmerPojo.setLong2(myStore.get(offlineViewHolder.getAdapterPosition()).getLong2());
                    fullFarmerPojo.setLong3(myStore.get(offlineViewHolder.getAdapterPosition()).getLong3());
                    fullFarmerPojo.setLong4(myStore.get(offlineViewHolder.getAdapterPosition()).getLong4());
                    fullFarmerPojo.setUploadedToParent(false);
                    fullFarmerPojo.setNextOfKin(myStore.get(offlineViewHolder.getAdapterPosition()).getNextOfKin());
                    fullFarmerPojo.setNextOfKinPhone(myStore.get(offlineViewHolder.getAdapterPosition()).getNextOfKinPhone());
                    fullFarmerPojo.setTimeStamp(String.valueOf(new Date().getTime()));
                    intent.putExtra("data", fullFarmerPojo);
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            return myStore.size();
        }

        public class OfflineViewHolder extends RecyclerView.ViewHolder {
                CircleImageView imageView;
                TextView nameText, text1, text2, textGovt, textSyned;
                ImageView caller;
            public OfflineViewHolder(@NonNull View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.offline_dp);
                nameText = itemView.findViewById(R.id.offline_name);
                text1 = itemView.findViewById(R.id.another1);
                text2 = itemView.findViewById(R.id.another2);
                textGovt = itemView.findViewById(R.id.another3);
                textSyned = itemView.findViewById(R.id.synced);
                caller = itemView.findViewById(R.id.callertune);
            }
        }
    }
    void caller(String number) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
                return;

            }
            startActivity(callIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e("Calling a Phone Number", "Call failed", activityException);
        }
    }
    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PHONE_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(ViewOfflineFarmers.this, "Tap the call button again", Toast.LENGTH_SHORT).show();
                } else {

                }
                return;
            }
        }
    }

    void popUpDialog(String farmerId) {

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setTitle("Choose an action:");

        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_singlechoice);
        arrayAdapter.add("Edit Bio data");
        arrayAdapter.add("Edit bank and identity");
        arrayAdapter.add("Edit farming history");
        arrayAdapter.add("Edit Geolocation");

        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                dialog.cancel();
                switch (which) {
                    case 0:  Intent intent = new Intent(ViewOfflineFarmers.this, EditProfileActivity.class);
                    intent.putExtra("data", farmerId);
                    startActivity(intent);
                        break;
                    case 1: Intent intent2 = new Intent(ViewOfflineFarmers.this, EditBankDetails.class);
                        intent2.putExtra("data", farmerId);
                        startActivity(intent2);
                        break;
                    case 2: Intent intent3 = new Intent(ViewOfflineFarmers.this, EditHistory.class);
                        intent3.putExtra("data", farmerId);
                        startActivity(intent3);
                        break;
                    case 3: Intent intent4 = new Intent(ViewOfflineFarmers.this, GeoMappingActivity.class);
                        intent4.putExtra("data", farmerId);
                        startActivity(intent4);
                        break;
                }
            }
        });
        builderSingle.show();
    }
}
