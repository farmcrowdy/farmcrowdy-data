package com.farmcrowdy.farmcrowdydata.ListPojo;


import com.farmcrowdy.farmcrowdydata.DirectPojo.FarmGroupPojo;

public class FarmGroupSinglePojo {
    String status;
    FarmGroupPojo message;

    public String getStatus() {
        return status;
    }

    public FarmGroupPojo getMessage() {
        return message;
    }
}
