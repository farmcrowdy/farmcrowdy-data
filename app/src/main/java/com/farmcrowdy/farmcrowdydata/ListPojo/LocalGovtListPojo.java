package com.farmcrowdy.farmcrowdydata.ListPojo;


import com.farmcrowdy.farmcrowdydata.DirectPojo.LocalGovtPojo;

import java.util.ArrayList;

/**
 * Created by Oluwatobi on 5/24/2018.
 */

public class LocalGovtListPojo {
String status;
    ArrayList<LocalGovtPojo> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<LocalGovtPojo> getMessage() {
        return message;
    }
}
