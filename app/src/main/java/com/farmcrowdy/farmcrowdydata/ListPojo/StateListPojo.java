package com.farmcrowdy.farmcrowdydata.ListPojo;


import com.farmcrowdy.farmcrowdydata.DirectPojo.StatePojo;

import java.util.ArrayList;

/**
 * Created by Oluwatobi on 5/24/2018.
 */

public class StateListPojo {
    String status;
    ArrayList<StatePojo> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<StatePojo> getMessage() {
        return message;
    }

}
