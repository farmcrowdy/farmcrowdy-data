package com.farmcrowdy.farmcrowdydata.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.signUp.SignupFragment;


/**
 * Created by Oluwatobi on 5/28/2018.
 */

public class LoginActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //startActivity(new Intent(this, MassCheck.class));
        setContentView(R.layout.get_in_activity);
        ViewPager viewPager = (ViewPager)findViewById(R.id.login_viewPager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.login_tab);
        viewPager.setAdapter(new MyViewPager(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
    }

    private class MyViewPager extends FragmentPagerAdapter {

        public MyViewPager(FragmentManager fm) {
            super(fm);
        }
        private String[] tabTitles = {"Login", "Register"};

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = new Fragment();
            switch (position){
                case 0: fragment = new LoginFragment();
                    break;
                case 1: fragment = new SignupFragment();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
