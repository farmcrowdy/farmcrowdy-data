package com.farmcrowdy.farmcrowdydata.login;

/**
 * Created by Oluwatobi on 5/25/2018.
 */

public interface LoginPresenter {
    void loginNow(String email, String password);
}
