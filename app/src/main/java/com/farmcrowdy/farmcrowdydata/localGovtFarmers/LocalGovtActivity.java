package com.farmcrowdy.farmcrowdydata.localGovtFarmers;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.login.LoginActivity;
import com.farmcrowdy.farmcrowdydata.login.LoginFragment;
import com.farmcrowdy.farmcrowdydata.signUp.SignupFragment;

public class LocalGovtActivity extends AppCompatActivity{
    private static String localGovtString;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.local_govt_activity);

        setLocalGovtString(getIntent().getExtras().getString("data"));

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        ViewPager viewPager = findViewById(R.id.login_viewPager);
        TabLayout tabLayout = findViewById(R.id.login_tab);
        viewPager.setAdapter(new MyViewPager(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);

    }

    public static String getLocalGovtString() {
        return localGovtString;
    }

    public static void setLocalGovtString(String localGovtString) {
        LocalGovtActivity.localGovtString = localGovtString;
    }

    private class MyViewPager extends FragmentPagerAdapter {

        public MyViewPager(FragmentManager fm) {
            super(fm);
        }
        private String[] tabTitles = {"Chairmen", "Secretary", "Members"};

        @Override
        public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = new Fragment();
            switch (position){
                case 0: fragment = new ChairmanFragment();
                    break;
                case 1: fragment = new SecretaryFragment();
                    break;
                case 2: fragment = new MembersFragment();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}
