package com.farmcrowdy.farmcrowdydata.localGovtFarmers;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.farmcrowdy.farmcrowdydata.ApplicationInstance;
import com.farmcrowdy.farmcrowdydata.GeoMappingActivity;
import com.farmcrowdy.farmcrowdydata.R;
import com.farmcrowdy.farmcrowdydata.addFarmer.FullFarmerPojo;
import com.farmcrowdy.farmcrowdydata.dashboard.SingleFarmerProfileActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SecretaryFragment  extends Fragment{
    RecyclerView recyclerView;
    private static final int REQUEST_PHONE_CALL = 300;
    ArrayList<FullFarmerPojo> myStore = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.view_offline_farmers, container, false);
        view.findViewById(R.id.toolbar).setVisibility(View.GONE);

        for(FullFarmerPojo pojo: ApplicationInstance.getFullFarmerPojoList()) {
            if(pojo.getGovtString().equals(LocalGovtActivity.getLocalGovtString()) && pojo.getRole().equals("Secretary")) myStore.add(pojo);
        }


        recyclerView = view.findViewById(R.id.offlineFarmersRecyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new OfflineRecyclerAdapter());

        return view;
    }

    private class OfflineRecyclerAdapter extends RecyclerView.Adapter<OfflineRecyclerAdapter.OfflineViewHolder> {
        @NonNull
        @Override
        public OfflineRecyclerAdapter.OfflineViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = getLayoutInflater().inflate(R.layout.offline_farmers_items, viewGroup, false);
            return new OfflineRecyclerAdapter.OfflineViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final OfflineRecyclerAdapter.OfflineViewHolder offlineViewHolder, int i) {
            offlineViewHolder.nameText.setText(myStore.get(i).getFname());
            offlineViewHolder.text1.setText(myStore.get(i).getFarmerId());
            offlineViewHolder.text2.setText(myStore.get(i).getCrop_proficiency()+" Farmer");
            offlineViewHolder.textGovt.setText("Location: "+myStore.get(i).getGovtString());
            offlineViewHolder.textSyned.setText("Data backed up online");
            offlineViewHolder.caller.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    caller(myStore.get(offlineViewHolder.getAdapterPosition()).getPhone());
                }
            });
            Picasso.with(getContext()).load(myStore.get(i).getPro_image()).placeholder(R.drawable.ic_accout_blue_24dp).into(offlineViewHolder.imageView);
            //offlineViewHolder.imageView.setImageURI(Uri.parse(""));

            offlineViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getContext(), SingleFarmerProfileActivity.class);
                    intent.putExtra("data", myStore.get(offlineViewHolder.getAdapterPosition()));
                    startActivity(intent);
                }
            });

        }

        @Override
        public int getItemCount() {
            return myStore.size();
        }

        public class OfflineViewHolder extends RecyclerView.ViewHolder {
            CircleImageView imageView;
            TextView nameText, text1, text2, textGovt, textSyned;
            ImageView caller;
            public OfflineViewHolder(@NonNull View itemView) {
                super(itemView);
                imageView = itemView.findViewById(R.id.offline_dp);
                nameText = itemView.findViewById(R.id.offline_name);
                text1 = itemView.findViewById(R.id.another1);
                text2 = itemView.findViewById(R.id.another2);
                textGovt = itemView.findViewById(R.id.another3);
                textSyned = itemView.findViewById(R.id.synced);
                caller = itemView.findViewById(R.id.callertune);
            }
        }
    }
    void caller(String number) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + number));
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE},REQUEST_PHONE_CALL);
                return;

            }
            startActivity(callIntent);
        } catch (ActivityNotFoundException activityException) {
            Log.e("Calling a Phone Number", "Call failed", activityException);
        }
    }
}
