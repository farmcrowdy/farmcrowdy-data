package com.farmcrowdy.farmcrowdydata;

import com.farmcrowdy.farmcrowdydata.DirectPojo.TFS_userPojo;
import com.farmcrowdy.farmcrowdydata.ListPojo.CropListPojo;
import com.farmcrowdy.farmcrowdydata.ListPojo.FarmGroupListPojo;
import com.farmcrowdy.farmcrowdydata.ListPojo.FarmGroupSinglePojo;
import com.farmcrowdy.farmcrowdydata.ListPojo.FarmLocationListPojo;
import com.farmcrowdy.farmcrowdydata.ListPojo.FarmerListPojo;
import com.farmcrowdy.farmcrowdydata.ListPojo.GetBanksListPojo;
import com.farmcrowdy.farmcrowdydata.ListPojo.LocalGovtListPojo;
import com.farmcrowdy.farmcrowdydata.ListPojo.StateListPojo;
import com.farmcrowdy.farmcrowdydata.dashboard.ListOfAgentsPojo;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by Oluwatobi on 5/23/2018.
 */

public interface MyEndpoint {


    @Multipart
    @POST("farmerprint/{id}")
    Call<ResponseBody> updateFingerPrint(@Path("id") int farmerId,
                                         @Part("f_print") RequestBody fingerprintName);

    @Multipart
    @POST("addimage")
    Call<ResponseBody> upload(
            @Part MultipartBody.Part pimage, @Part("pimage") RequestBody name
    );


    @Multipart
    @POST("userlogin/")
    Call<LoginPojo> login(@Part("email") RequestBody email, @Part("password") RequestBody password);

    @Multipart
    @POST("user")
    Call<ResponseBody> addNewUser(@Part("email") RequestBody email, @Part("password") RequestBody password,
                                  @Part("firstname") RequestBody firstname, @Part("surname") RequestBody surname);
    @GET("user")
    Call<ListOfAgentsPojo> getAllAgents();

    @GET("user/{id}")
    Call<TFS_userPojo> getTFSInfoById(@Path("id") int id);

    @Multipart
    @POST("farmUpdate")
    Call<ResponseBody> sendFarmUpdate(@Part("tfs_id") int tfs_id,
                                      @Part("farm_groupid") int farmgroupId,
                                      @Part("subject") RequestBody subject,
                                      @Part("comment") RequestBody comment,
                                      @Part("farm_image") RequestBody farm_image,
                                      @Part("farm_video") RequestBody farm_video,
                                      @Part MultipartBody.Part updatevideo,
                                      @Part MultipartBody.Part pimage);


    @Multipart
    @POST("flagfarmer/{id}")
    Call<ResponseBody> sendFlagFarmer(@Path("id") int farmerKey, @Part("flag_comment") RequestBody comment);

    @GET("farmgroup/{id}")
    Call<FarmGroupSinglePojo> getFarmGroupById(@Path("id") int id);

    @GET("farmersearch/{search}")
    Call<FarmerListPojo> getFarmerList(@Path("search") String search);

    @GET("farmers")
    Call<FarmerListPojo> getAllFarmers();

    @GET("farmgroupsearch/{search}")
    Call<FarmGroupListPojo> getListOfFarmGroupByKeyword(@Path("search") String search);

    @GET("banks")
    Call<GetBanksListPojo> getAllBanks();

    @GET("farmlocation")
    Call<FarmLocationListPojo> getAllFarmLocations();

    @Multipart
    @POST("input")
    Call<ResponseBody> addInputInventory(@Part("user_id") int userID, @Part("crop_id") int cropId,
                                         @Part("input_name") RequestBody inputName,
                                         @Part("total_quantity") int totalQuanity,
                                         @Part("input_unit") RequestBody inputUnit,
                                         @Part("seller_name") RequestBody sellerName);


    @Multipart
    @POST("farmerinput/{id}")
    Call<ResponseBody> assignInputToFarmer(@Path("id") int id, @Part("farmer_id") int farmerId, @Part("user_assign_id") int tfs_id,
                                           @Part("farmer_input_id") int farmerInputId, @Part("farmer_input_quantity") int inputQuantity,
                                           @Part("planting_stage") RequestBody plantingStage);
    @Multipart
    @POST("monitor/{id}")
    Call<ResponseBody> doMonitor(@Path("id") int id, @Part("user_monitor_id") int tfs_id, @Part("farmer_monitor_id") int farmerId,
                                 @Part("crop_monitor_type")int  cropId, @Part("farmgroup_monitor_id") int farmGroupId,
                                 @Part("date_of_visit") RequestBody dateOfVisit, @Part("monitor_comment") RequestBody comment);

    @Multipart
    @POST("cropmonitor")
    Call<ResponseBody> doCropMonitor(@Part("tfs_user_id") int tfs_user_id, @Part("farmer_id") int farmer_id,
                                     @Part("crop_type") int cropTypeId, @Part("farming_stage") RequestBody farming_stage,
                                     @Part("q1") int landFertile, @Part("q2") int flood_and_disaster,
                                     @Part("q3") int external_threats,

                                     @Part MultipartBody.Part pimage,
                                     @Part("photo_string") RequestBody pro_image,

                                     @Part("q6") int signs_of_scratching,
                                     @Part("q7") int regular_feed_habit, @Part("q8") int regular_growth_habit,
                                     @Part("q9") int good_appereance, @Part("age_of_bird") RequestBody age_of_bird,
                                     @Part("weight_of_birds") RequestBody weight_of_birds, @Part("poultry_state") RequestBody poultry_state,
                                     @Part("mon_comment") RequestBody mon_comment,
                                     @Part("q4") int q4, @Part("q5") int q5);

    @Multipart
    @POST("assignLand/{id}")
    Call<ResponseBody> assignLand(@Path("id") int id, @Part("user_assign_land_id") int user_assign_land_id,
                                  @Part("farmer_assign_land_id") int farmer_assign_land_id, @Part("land_area") RequestBody land_area,
                                  @Part("latlonga") RequestBody latlonga,
                                  @Part("latlongb") RequestBody latlongb,
                                  @Part("latlongc") RequestBody latlongc,
                                  @Part("latlongd") RequestBody latlongd);

    @GET("localgovt")
    Call<LocalGovtListPojo> getAllLocalGov();

    @GET("states")

    Call<StateListPojo> getAllStates();

    @GET("crops")
    Call<CropListPojo> getAllCrops();



    @GET("farmersByGroup/{id}")
    Call<FarmerListPojo> getAllFarmerInAFarmGroup(@Path("id") int id);

    @GET("farmgroup")
    Call<FarmGroupListPojo> getAllFarmGroups();

    @Multipart
    @POST("harvestCollection")
    Call<ResponseBody> sendHarvestCollection(@Part("user_collect_id") int user_collect_id,
                                             @Part("farmer_collect_id") int farmer_collect_id,
                                             @Part("weight_of_bird") RequestBody weight_of_bird,
                                             @Part("num_of_mortality") int num_of_mortality,
                                             @Part("crop_type") RequestBody crop_type,
                                             @Part("quantity") int quantity,
                                             @Part("quantity_unit") RequestBody quantity_unit,
                                             @Part("comment") RequestBody comment);

    @Multipart
    @POST("farmgroup")
    Call<ResponseBody> addNewGroup(@Part("group_name") RequestBody groupName,
                                   @Part("gr_crop_type")RequestBody cropType,
                                   @Part("cycle_duration")RequestBody cycleDuraton,
                                   @Part("land_area_assign") int landAssignArea,
                                   @Part("land_area_unit")RequestBody unit,
                                   @Part("gr_location_id") int location_id);



    @Multipart
    @POST("updatefarmer/{id}")
    Call<ResponseBody>updateFarmer(
            @Path("id") int id,
            @Part("user_add_id") int tfs_id,
            @Part("fname") RequestBody fname,
            @Part("sname") RequestBody sname,
            @Part("dob") RequestBody dob,
            @Part("state_id") int stateId,
            @Part("local_id") int local_id,
            @Part("farm_group_id") int farm_group_id,
            @Part("farm_location_id") int farm_location_id,
            @Part("bank_id") int bank_id,
            @Part("gender") RequestBody gender,
            @Part("phone") RequestBody phone,
            @Part("marital_status") RequestBody marital_status,
            @Part("number_of_dependants") int number_of_dependant,
            @Part("add_labour") int add_labour,
            @Part("crop_proficiency") RequestBody crop_proficiency,
            @Part("acct_number") RequestBody acct_number,
            @Part("land_area_farmed") RequestBody land_area_farmed,
            @Part("years_of_experience") int years_of_experience,
            @Part("income_range") RequestBody income_range,
            @Part("previous_training") int previous_training,
            @Part("comment") RequestBody comment);

    @Multipart
    @POST("nfarmers")
    Call<ResponseBody> publicFarmers(
            @Part("fname") RequestBody fname,
            @Part("role") RequestBody role,
            @Part("lat1") RequestBody lat1,
            @Part("lat2") RequestBody lat2,
            @Part("lat3") RequestBody lat3,
            @Part("lat4") RequestBody lat4,
            @Part("long1") RequestBody long1,
            @Part("long2") RequestBody long2,
            @Part("long3") RequestBody long3,
            @Part("long4") RequestBody long4,
            @Part("farmerId") RequestBody farmerId,
            @Part("timeStamp") RequestBody timeStamp,
            @Part("govtString") RequestBody govtString,
            @Part("farm_group_id") RequestBody farm_group_id,
            @Part("bankName") RequestBody bankName,
            @Part("bvnNumber") RequestBody bvnNumber,
            @Part("idType") RequestBody idType,
            @Part("idNumber") RequestBody idNumber,
            @Part("dob") RequestBody dob,
            @Part("gender") RequestBody gender,
            @Part("phone") RequestBody phone,
            @Part("marital_status") RequestBody marital_status,
            @Part("crop_proficiency") RequestBody crop_proficiency,
            @Part("income_range") RequestBody income_range,
            @Part("pro_image") RequestBody pro_image,
            @Part("pro_image_thumbnail") RequestBody imageForIdentification,
            @Part("acct_number") RequestBody acct_number,
            @Part("land_area_farmed") RequestBody land_area_farmed,
            @Part("nextOfKin") RequestBody nextOfKin,
            @Part("nextOfKinPhone") RequestBody nextOfKinPhone,
            @Part("user_add_id") int user_add_id,
            @Part("state_id") int state_id,
            @Part("local_id") int local_id,
            @Part("farm_location_id") int farm_location_id,
            @Part("bank_id") int bank_id,
            @Part("number_of_dependants") int number_of_dependants,
            @Part("years_of_experience") int years_of_experience,
            @Part("uploadedToParent") RequestBody uploadedToParent,
            @Part("readWriteEnglish") RequestBody readWriteEnglish,
            @Part("haveSmartPhone") RequestBody haveSmartPhone,
            @Part("sourceOfData") int sourceOfData,
            @Part("f_print") RequestBody fPrint
    );


    @GET("nfarmers")
    Call<ListFullfarmer> getFarmersFromAzure();

    @GET("nfarmersbyagent/{id}")
    Call<ListFullfarmer> getFarmersByAgentFromAzure(@Path("id") int id);





}
