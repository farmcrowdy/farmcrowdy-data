package com.farmcrowdy.farmcrowdydata;

import com.farmcrowdy.farmcrowdydata.addFarmer.FullFarmerPojo;

import java.util.ArrayList;

public class ListFullfarmer {
    String status;
    ArrayList<FullFarmerPojo> message;

    public String getStatus() {
        return status;
    }

    public ArrayList<FullFarmerPojo> getMessage() {
        return message;
    }
}
